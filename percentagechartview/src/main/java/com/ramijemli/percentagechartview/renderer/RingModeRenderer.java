/*
 * Copyright 2018 Rami Jemli
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ramijemli.percentagechartview.renderer;


import com.ramijemli.percentagechartview.IPercentageChartView;
import com.ramijemli.percentagechartview.callback.AdaptiveColorProvider;
import com.ramijemli.percentagechartview.utils.ArgbEvaluator;
import com.ramijemli.percentagechartview.utils.MyAnimatorValue;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.render.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;

import java.util.Arrays;
import java.util.stream.Collectors;

import static com.ramijemli.percentagechartview.utils.MyAttrsUtils.*;
import static ohos.agp.render.Paint.StrokeCap.BUTT_CAP;
import static ohos.agp.render.Paint.StrokeCap.ROUND_CAP;


public class RingModeRenderer extends BaseModeRenderer implements OrientationBasedMode {

    // BACKGROUND BAR
    private static final float DEFAULT_BG_BAR_DP_WIDTH = 16;

    private Paint mBackgroundBarPaint;
    private boolean mDrawBackgroundBar;
    private float mBackgroundBarThickness;
    private int mBackgroundBarColor;
    private int mProvidedBgBarColor;

    //PROGRESS BAR
    private static final float DEFAULT_PROGRESS_BAR_DP_WIDTH = 16;
    public static final int CAP_ROUND = 0;
    public static final int CAP_SQUARE = 1;

    private Paint.StrokeCap mProgressBarStyle;
    private float mProgressBarThickness;

    //TO PUSH PROGRESS BAR OUT OF SWEEP GRADIENT'S WAY
    private float tweakAngle;

    public RingModeRenderer(IPercentageChartView view) {
        super(view);
        init();
        setup();
    }

    public RingModeRenderer(IPercentageChartView view, AttrSet attrs) {
        super(view, attrs);
        init(attrs);
        setup();
    }

    private void init(AttrSet attrs) {
        //BACKGROUND BAR DRAW STATE
        mDrawBackgroundBar = true;
        if(attrs.getAttr(PCV_DRAW_BACKGROUND_BAR).isPresent()) {
            mDrawBackgroundBar = attrs.getAttr(PCV_DRAW_BACKGROUND_BAR).get().getBoolValue();
        }

        //BACKGROUND WIDTH
        mBackgroundBarThickness = AttrHelper.vp2px(DEFAULT_BG_BAR_DP_WIDTH,mView.getViewContext());
        if(attrs.getAttr(PCV_BACKGROUND_BAR_THICKNESS).isPresent()) {
            mBackgroundBarThickness = attrs.getAttr(PCV_BACKGROUND_BAR_THICKNESS).get().getDimensionValue();
        }

        //BACKGROUND BAR COLOR
        mBackgroundBarColor = Color.BLACK.getValue();
        if(attrs.getAttr(PCV_BACKGROUND_BAR_COLOR).isPresent()) {
            mBackgroundBarColor = attrs.getAttr(PCV_BACKGROUND_BAR_COLOR).get().getColorValue().getValue();
        }

        //PROGRESS WIDTH
        mProgressBarThickness = AttrHelper.vp2px(DEFAULT_PROGRESS_BAR_DP_WIDTH,mView.getViewContext());
        if(attrs.getAttr(PCV_PROGRESS_BAR_THICKNESS).isPresent()) {
            mProgressBarThickness = attrs.getAttr(PCV_PROGRESS_BAR_THICKNESS).get().getDimensionValue();
        }

        //PROGRESS BAR STROKE STYLE
        int cap = CAP_ROUND;
        if(attrs.getAttr(PCV_PROGRESS_BAR_STYLE).isPresent()) {
            cap = attrs.getAttr(PCV_PROGRESS_BAR_STYLE).get().getIntegerValue();
        }

        mProgressBarStyle = (cap==CAP_ROUND) ? ROUND_CAP : BUTT_CAP;
    }

    private void init() {
        //DRAW BACKGROUND BAR
        mDrawBackgroundBar = true;

        //BACKGROUND WIDTH
        mBackgroundBarThickness = AttrHelper.vp2px(DEFAULT_BG_BAR_DP_WIDTH,mView.getViewContext());

        //BACKGROUND BAR COLOR
        mBackgroundBarColor = Color.BLACK.getValue();

        //PROGRESS BAR WIDTH
        mProgressBarThickness = AttrHelper.vp2px(DEFAULT_PROGRESS_BAR_DP_WIDTH,mView.getViewContext());

        //PROGRESS BAR STROKE STYLE
        mProgressBarStyle = ROUND_CAP;
    }

    @Override
    void setup() {
        super.setup();
        mProvidedBgBarColor = -1;
        tweakAngle = 0;
        updateDrawingAngles();

        //BACKGROUND BAR
        mBackgroundBarPaint = new Paint();
        mBackgroundBarPaint.setAntiAlias(true);
        mBackgroundBarPaint.setStyle(Paint.Style.STROKE_STYLE);
        mBackgroundBarPaint.setColor(new Color(mBackgroundBarColor));
        mBackgroundBarPaint.setStrokeWidth(mBackgroundBarThickness);
        mBackgroundBarPaint.setStrokeCap(mProgressBarStyle);

        //PROGRESS PAINT
        mProgressPaint.setStyle(Paint.Style.STROKE_STYLE);
        mProgressPaint.setStrokeWidth(mProgressBarThickness);
        mProgressPaint.setStrokeCap(mProgressBarStyle);
    }

    @Override
    public void measure(int w, int h, int paddingLeft, int paddingTop, int paddingRight, int paddingBottom) {
        int diameter = Math.min(w, h);
        float maxOffset = Math.max(mProgressBarThickness, mBackgroundBarThickness);

        int centerX = w / 2;
        int centerY = h / 2;
        float radius = (diameter - maxOffset) / 2;

        mCircleBounds.modify(centerX - radius,
                centerY - radius,
                centerX + radius,
                centerY + radius);

        float backgroundRadius = radius - (mBackgroundBarThickness / 2) + 1;

        mBackgroundBounds.modify(centerX - backgroundRadius,
                centerY - backgroundRadius,
                centerX + backgroundRadius,
                centerY + backgroundRadius);

        setupGradientColors(mCircleBounds);
        updateText();
    }

    @Override
    public void draw(Canvas canvas) {
        //BACKGROUND
        if (mDrawBackground) {
            canvas.drawArc(mBackgroundBounds, new Arc(0, 360, false) , mBackgroundPaint);
        }

        //BACKGROUND BAR
        if (mDrawBackgroundBar) {
            if (mBackgroundBarThickness <= mProgressBarThickness) {
                canvas.drawArc(mCircleBounds, new Arc(mStartAngle + tweakAngle, -(360 - mSweepAngle + tweakAngle), false), mBackgroundBarPaint);
            } else {
                canvas.drawArc(mCircleBounds, new Arc(0, 360, false), mBackgroundBarPaint);
            }
        }

        //FOREGROUND
        if (mProgress != 0) {
            canvas.drawArc(mCircleBounds, new Arc(mStartAngle + tweakAngle, mSweepAngle, false) , mProgressPaint);
        }

        //TEXT
        drawText(canvas);
    }

    @Override
    public void destroy() {
        super.destroy();
        if (mBgBarColorAnimator != null) {
            if (mBgBarColorAnimator.isRunning()) {
                mBgBarColorAnimator.cancel();
            }
            mBgBarColorAnimator.setValueUpdateListener(null);
        }
        mBgBarColorAnimator = null;
        mBackgroundBarPaint = null;
    }

    @Override
    public void setAdaptiveColorProvider( AdaptiveColorProvider adaptiveColorProvider) {
        if (adaptiveColorProvider == null) {
            mProgressColorAnimator = mBackgroundColorAnimator = mTextColorAnimator = mBgBarColorAnimator = null;
            this.mAdaptiveColorProvider = null;
            mTextPaint.setColor(new Color(mTextColor));
            mBackgroundBarPaint.setColor(new Color(mBackgroundBarColor));
            mBackgroundPaint.setColor(new Color(mBackgroundColor));
            mProgressPaint.setColor(new Color(mProgressColor));
            mView.postInvalidate();
            return;
        }

        this.mAdaptiveColorProvider = adaptiveColorProvider;

        setupColorAnimations();
        updateProvidedColors(mProgress);
        mView.postInvalidate();
    }

    @Override
    void setupGradientColors(RectFloat bounds) {
        if (mGradientType == -1) return;

        double ab = Math.pow(bounds.bottom - bounds.getCenter().getPointY(), 2);
        tweakAngle = (float) Math.toDegrees(Math.acos((2 * ab - Math.pow(mProgressBarThickness / 2, 2)) / (2 * ab)));

        switch (mGradientType) {
            default:
            case GRADIENT_LINEAR:
                mGradientShader = new LinearShader(new Point[]{ new Point(bounds.getCenter().getPointX(), bounds.top), new Point(bounds.getCenter().getPointX(),
                        bounds.bottom) },
                        mGradientDistributions,
                        Arrays.stream(mGradientColors).mapToObj(colorInt -> new Color(colorInt)).collect(Collectors.toList()).toArray(new Color[0]),
                        Shader.TileMode.CLAMP_TILEMODE
                );
                updateGradientAngle(mStartAngle);
                mProgressPaint.setShader(mGradientShader, Paint.ShaderType.LINEAR_SHADER);
                break;

            case GRADIENT_RADIAL:
                mGradientShader = new RadialShader(new Point(bounds.getCenter().getPointX(), bounds.getCenter().getPointY()),
                        bounds.bottom - bounds.getCenter().getPointY(),
                        mGradientDistributions,
                        Arrays.stream(mGradientColors).mapToObj(colorInt -> new Color(colorInt)).collect(Collectors.toList()).toArray(new Color[0]),
                        Shader.TileMode.MIRROR_TILEMODE);
                mProgressPaint.setShader(mGradientShader, Paint.ShaderType.RADIAL_SHADER);
                break;

            case GRADIENT_SWEEP:
                mGradientShader = new SweepShader(bounds.getCenter().getPointX(), bounds.getCenter().getPointY(),
                        Arrays.stream(mGradientColors).mapToObj(colorInt -> new Color(colorInt)).collect(Collectors.toList()).toArray(new Color[0]),
                        mGradientDistributions);

                if (!mView.isInEditMode()) {
                    // THIS BREAKS SWEEP GRADIENT'S PREVIEW MODE
                    updateGradientAngle(mStartAngle);
                }
                mProgressPaint.setShader(mGradientShader, Paint.ShaderType.SWEEP_SHADER);
                break;
        }

    }

    @Override
    void setupColorAnimations() {
        super.setupColorAnimations();
        if (mBgBarColorAnimator == null) {
            mBgBarColorAnimator = MyAnimatorValue.ofObject(new ArgbEvaluator(), mBackgroundBarColor, mProvidedBgBarColor);
            mBgBarColorAnimator.setValueUpdateListener((animation,v) -> {
                mProvidedBgBarColor = (int) ((MyAnimatorValue)animation).getAnimatedValue();
                mBackgroundBarPaint.setColor(new Color(mProvidedBgBarColor));
            });
            mBgBarColorAnimator.setDuration(mAnimDuration);
        }
    }

    @Override
    void cancelAnimations() {
        super.cancelAnimations();
        if (mBgBarColorAnimator != null && mBgBarColorAnimator.isRunning()) {
            mBgBarColorAnimator.cancel();
        }
    }

    @Override
    void updateAnimations(float progress) {
        super.updateAnimations(progress);

        if (mAdaptiveColorProvider == null) return;

        int providedBgBarColor = mAdaptiveColorProvider.provideBackgroundBarColor(progress);
        if (providedBgBarColor != -1 && providedBgBarColor != mProvidedBgBarColor) {
            int startColor = mProvidedBgBarColor!= -1 ? mProvidedBgBarColor : mBackgroundBarColor;
            mBgBarColorAnimator.setIntValues(startColor, providedBgBarColor);
            mBgBarColorAnimator.start();
        }
    }

    @Override
    void updateProvidedColors(float progress) {
        super.updateProvidedColors(progress);
        if (mAdaptiveColorProvider == null) return;
        int providedBgBarColor = mAdaptiveColorProvider.provideBackgroundBarColor(progress);
        if (providedBgBarColor != -1 && providedBgBarColor != mProvidedBgBarColor) {
            mProvidedBgBarColor = providedBgBarColor;
            mBackgroundBarPaint.setColor(new Color(mProvidedBgBarColor));
        }
    }

    @Override
    void updateDrawingAngles() {
        switch (orientation) {
            case ORIENTATION_COUNTERCLOCKWISE:
                mSweepAngle = -(mProgress / DEFAULT_MAX * 360);
                break;

            default:
            case ORIENTATION_CLOCKWISE:
                mSweepAngle = mProgress / DEFAULT_MAX * 360;
                break;
        }
    }

    @Override
    void updateGradientAngle(float angle) {
        if (mGradientType == -1 || mGradientType == GRADIENT_RADIAL) return;
        Matrix matrix = new Matrix();
        matrix.postRotate(angle, mCircleBounds.getCenter().getPointX(), mCircleBounds.getCenter().getPointY());
        mGradientShader.setShaderMatrix(matrix);
    }

    public int getOrientation() {
        return orientation;
    }

    public void setOrientation(int orientation) {
        if (this.orientation == orientation) return;
        this.orientation = orientation;
        updateDrawingAngles();
    }

    @Override
    public void setStartAngle(float startAngle) {
        if (this.mStartAngle == startAngle) return;
        this.mStartAngle = startAngle;
        if (mGradientType == GRADIENT_SWEEP) {
            updateGradientAngle(startAngle);
        }
    }

    // DRAW BACKGROUND BAR STATE
    public boolean isDrawBackgroundBarEnabled() {
        return mDrawBackgroundBar;
    }

    public void setDrawBackgroundBarEnabled(boolean drawBackgroundBar) {
        if (mDrawBackgroundBar == drawBackgroundBar) return;
        this.mDrawBackgroundBar = drawBackgroundBar;
    }

    //BACKGROUND BAR COLOR
    public int getBackgroundBarColor() {
        if (!mDrawBackgroundBar) return -1;
        return mBackgroundBarColor;
    }

    public void setBackgroundBarColor(int backgroundBarColor) {
        if (!mDrawBackgroundBar || (mAdaptiveColorProvider != null && mAdaptiveColorProvider.provideBackgroundBarColor(mProgress) != -1) || this.mBackgroundBarColor == backgroundBarColor)
            return;
        this.mBackgroundBarColor = backgroundBarColor;
        mBackgroundBarPaint.setColor(new Color(mBackgroundBarColor));
    }

    //BACKGROUND BAR THICKNESS
    public float getBackgroundBarThickness() {
        return mBackgroundBarThickness;
    }

    public void setBackgroundBarThickness(float backgroundBarThickness) {
        if (this.mBackgroundBarThickness == backgroundBarThickness) return;
        this.mBackgroundBarThickness = backgroundBarThickness;
        mBackgroundBarPaint.setStrokeWidth(backgroundBarThickness);
        measure(mView.getWidth(), mView.getHeight(), 0, 0, 0, 0);
    }

    //PROGRESS BAR THICKNESS
    public float getProgressBarThickness() {
        return mProgressBarThickness;
    }

    public void setProgressBarThickness(float progressBarThickness) {
        if (this.mProgressBarThickness == progressBarThickness) return;
        this.mProgressBarThickness = progressBarThickness;
        mProgressPaint.setStrokeWidth(progressBarThickness);
        measure(mView.getWidth(), mView.getHeight(), 0, 0, 0, 0);
    }

    //PROGRESS BAR STYLE
    public int getProgressBarStyle() {
        return (mProgressBarStyle == ROUND_CAP) ? CAP_ROUND : CAP_SQUARE;
    }

    public void setProgressBarStyle(int progressBarStyle) {
        if (progressBarStyle < 0 || progressBarStyle > 1) {
            throw new IllegalArgumentException("Text style must be a valid TextStyle constant.");
        }
        mProgressBarStyle = (progressBarStyle == CAP_ROUND) ? ROUND_CAP : BUTT_CAP;
        mProgressPaint.setStrokeCap(mProgressBarStyle);
    }

}
