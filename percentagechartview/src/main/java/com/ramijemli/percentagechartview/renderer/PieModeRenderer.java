/*
 * Copyright 2018 Rami Jemli
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ramijemli.percentagechartview.renderer;

import com.ramijemli.percentagechartview.IPercentageChartView;
import com.ramijemli.percentagechartview.callback.AdaptiveColorProvider;
import ohos.agp.components.AttrSet;
import ohos.agp.render.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;

import java.util.Arrays;
import java.util.stream.Collectors;

import static com.ramijemli.percentagechartview.utils.MyAttrsUtils.*;

public class PieModeRenderer extends BaseModeRenderer implements OrientationBasedMode, OffsetEnabledMode {

    private float mBgStartAngle;
    private float mBgSweepAngle;

    public PieModeRenderer(IPercentageChartView view) {
        super(view);
        setup();
    }

    public PieModeRenderer(IPercentageChartView view, AttrSet attrs) {
        super(view, attrs);
        setup();
    }

    @Override
    void setup() {
        super.setup();
        updateDrawingAngles();
    }

    @Override
    public void measure(int w, int h, int paddingLeft, int paddingTop, int paddingRight, int paddingBottom) {
        float centerX = w * 0.5f;
        float centerY = h * 0.5f;
        float radius = Math.min(w, h) * 0.5f;

        mCircleBounds.modify(centerX - radius,
                centerY - radius,
                centerX + radius,
                centerY + radius);
        measureBackgroundBounds();
        setupGradientColors(mCircleBounds);
        updateText();
    }

    private void measureBackgroundBounds() {
        mBackgroundBounds.modify(mCircleBounds.left + mBackgroundOffset,
                mCircleBounds.top + mBackgroundOffset,
                mCircleBounds.right - mBackgroundOffset,
                mCircleBounds.bottom - mBackgroundOffset);
    }

    @Override
    public void draw(Canvas canvas) {
        if (mGradientType == GRADIENT_SWEEP && mView.isInEditMode()) {
            // TO GET THE RIGHT DRAWING START ANGLE FOR SWEEP GRADIENT'S COLORS IN PREVIEW MODE
            canvas.save();
            canvas.rotate(mStartAngle, mCircleBounds.getCenter().getPointX(), mCircleBounds.getCenter().getPointY());
        }

        //FOREGROUND
        canvas.drawArc(mCircleBounds, new Arc(mStartAngle, mSweepAngle, true), mProgressPaint);

        //BACKGROUND
        if (mDrawBackground) {
            canvas.drawArc(mBackgroundBounds, new Arc(mBgStartAngle, mBgSweepAngle, true), mBackgroundPaint);
        }

        if (mGradientType == GRADIENT_SWEEP && mView.isInEditMode()) {
            // TO GET THE RIGHT DRAWING START ANGLE FOR SWEEP GRADIENT'S COLORS IN PREVIEW MODE
            canvas.restore();
        }

        //TEXT
        drawText(canvas);
    }

    @Override
    public void setAdaptiveColorProvider( AdaptiveColorProvider adaptiveColorProvider) {
        if (adaptiveColorProvider == null) {
            mProgressColorAnimator = mBackgroundColorAnimator = mTextColorAnimator = null;
            mAdaptiveColorProvider = null;
            mTextPaint.setColor(new Color(mTextColor));
            mBackgroundPaint.setColor(new Color(mBackgroundColor));
            mProgressPaint.setColor(new Color(mProgressColor));
            mView.postInvalidate();
            return;
        }

        this.mAdaptiveColorProvider = adaptiveColorProvider;

        setupColorAnimations();
        updateProvidedColors(mProgress);
        mView.postInvalidate();
    }

    @Override
    void setupGradientColors(RectFloat bounds) {
        if (mGradientType == -1 && bounds.getHeight() == 0) return;

        switch (mGradientType) {
            default:
            case GRADIENT_LINEAR:
                mGradientShader = new LinearShader(new Point[]{ new Point(bounds.getCenter().getPointX(), bounds.top),
                        new Point(bounds.getCenter().getPointX(), bounds.bottom)},
                        mGradientDistributions,
                        Arrays.stream(mGradientColors).mapToObj(colorInt -> new Color(colorInt)).collect(Collectors.toList()).toArray(new Color[0]),
                        Shader.TileMode.CLAMP_TILEMODE
                );
                updateGradientAngle(mGradientAngle);
                mProgressPaint.setShader(mGradientShader, Paint.ShaderType.LINEAR_SHADER);
                break;

            case GRADIENT_RADIAL:
                mGradientShader = new RadialShader(new Point(bounds.getCenter().getPointX(), bounds.getCenter().getPointY()),
                        bounds.bottom - bounds.getCenter().getPointY(),
                        mGradientDistributions,
                        Arrays.stream(mGradientColors).mapToObj(colorInt -> new Color(colorInt)).collect(Collectors.toList()).toArray(new Color[0]),
                        Shader.TileMode.MIRROR_TILEMODE);
                mProgressPaint.setShader(mGradientShader, Paint.ShaderType.RADIAL_SHADER);
                break;

            case GRADIENT_SWEEP:
                mGradientShader = new SweepShader(bounds.getCenter().getPointX(), bounds.getCenter().getPointY(),
                        Arrays.stream(mGradientColors).mapToObj(colorInt -> new Color(colorInt)).collect(Collectors.toList()).toArray(new Color[0]),
                        mGradientDistributions
                );

                if (!mView.isInEditMode()) {
                    // THIS BREAKS SWEEP GRADIENT'S PREVIEW MODE
                    updateGradientAngle(mStartAngle);
                }
                mProgressPaint.setShader(mGradientShader, Paint.ShaderType.SWEEP_SHADER);
                break;
        }

    }

    @Override
    void updateDrawingAngles() {
        switch (orientation) {
            case ORIENTATION_COUNTERCLOCKWISE:
                mSweepAngle = -(mProgress / DEFAULT_MAX * 360);
                mBgStartAngle = mStartAngle;
                mBgSweepAngle = 360 + mSweepAngle;
                break;

            default:
            case ORIENTATION_CLOCKWISE:
                mSweepAngle = mProgress / DEFAULT_MAX * 360;
                mBgStartAngle = mStartAngle + mSweepAngle;
                mBgSweepAngle = 360 - mSweepAngle;
                break;
        }
    }

    @Override
    void updateGradientAngle(float angle) {
        if (mGradientType == -1 || mGradientType == GRADIENT_RADIAL) return;
        Matrix matrix = new Matrix();
        matrix.postRotate(angle, mCircleBounds.getCenter().getPointX(), mCircleBounds.getCenter().getPointY());
        mGradientShader.setShaderMatrix(matrix);
    }

    public int getOrientation() {
        return orientation;
    }

    public void setOrientation(int orientation) {
        if (this.orientation == orientation) return;
        this.orientation = orientation;
        updateDrawingAngles();
    }

    @Override
    public void setStartAngle(float startAngle) {
        if (this.mStartAngle == startAngle) return;
        this.mStartAngle = startAngle;
        updateDrawingAngles();
        if (mGradientType == GRADIENT_SWEEP) {
            updateGradientAngle(startAngle);
        }
    }

    //BACKGROUND OFFSET
    public int getBackgroundOffset() {
        return mBackgroundOffset;
    }

    public void setBackgroundOffset(int backgroundOffset) {
        if (!mDrawBackground || this.mBackgroundOffset == backgroundOffset)
            return;
        this.mBackgroundOffset = backgroundOffset;
        measureBackgroundBounds();
    }

}
