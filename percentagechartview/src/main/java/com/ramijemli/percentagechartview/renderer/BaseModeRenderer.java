/*
 * Copyright 2018 Rami Jemli
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ramijemli.percentagechartview.renderer;

import com.ramijemli.percentagechartview.IPercentageChartView;
import com.ramijemli.percentagechartview.annotation.ProgressOrientation;
import com.ramijemli.percentagechartview.callback.AdaptiveColorProvider;
import com.ramijemli.percentagechartview.callback.ProgressTextFormatter;
import com.ramijemli.percentagechartview.utils.ArgbEvaluator;
import com.ramijemli.percentagechartview.utils.FontUtils;
import com.ramijemli.percentagechartview.utils.MyAnimatorValue;
import ohos.agp.animation.Animator;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Text;
import ohos.agp.render.Canvas;
import ohos.agp.render.MaskFilter;
import ohos.agp.render.Paint;
import ohos.agp.render.Shader;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.agp.utils.TextAlignment;

import static com.ramijemli.percentagechartview.utils.MyAttrsUtils.*;


public abstract class BaseModeRenderer {


    // TEXT
    private static final float DEFAULT_TEXT_SP_SIZE = 12;

    //ANIMATIONS
    private static final int DEFAULT_ANIMATION_INTERPOLATOR = 0;

    private static final int DEFAULT_START_ANGLE = 0;
    private static final int DEFAULT_ANIMATION_DURATION = 400;
    static final float DEFAULT_MAX = 100;


    //##############################################################################################
    // BACKGROUND
    boolean mDrawBackground;
    Paint mBackgroundPaint;
    int mBackgroundColor;
    int mBackgroundOffset;

    private int mProvidedBackgroundColor;

    // PROGRESS
    Paint mProgressPaint;
    int mProgressColor;

    int colorOne = Color.getIntColor("#F44336");
    int colorTwo = Color.getIntColor("#FFEA00");
    int colorThree = Color.getIntColor("#03A9F4");
    int colorFour = Color.getIntColor("#00E676");

    int[] mGradientColors = new int[]{colorOne, colorTwo, colorThree, colorFour};
    float[] mGradientDistributions;
    int mGradientType;
    float mGradientAngle;
    Shader mGradientShader;

    // TEXT
    TextPaint mTextPaint;
    TextPaint mTextPaintShadow;
    int mTextColor;
    private int mProvidedTextColor;
    private int mTextProgress;
    private float mTextSize;
    private int mTextStyle;
    private Font.Builder mTypeface;
    private int mTextShadowColor;
    private float mTextShadowRadius;
    private float mTextShadowDistY;
    private float mTextShadowDistX;

//    private Editable mTextEditor;
//    private DynamicLayout mTextLayout;
    private Text mText;
    // COMMON
    RectFloat mBackgroundBounds;
    RectFloat mCircleBounds;
    MyAnimatorValue mProgressColorAnimator, mBackgroundColorAnimator, mTextColorAnimator, mBgBarColorAnimator;
    private MyAnimatorValue mProgressAnimator;
    private int mCurveType;
    int mAnimDuration;
    float mProgress;
    float mStartAngle;
    float mSweepAngle;

    private int mProvidedProgressColor;

    @ProgressOrientation
    int orientation;

    AdaptiveColorProvider mAdaptiveColorProvider;

    private ProgressTextFormatter mProvidedTextFormatter, defaultTextFormatter;

    IPercentageChartView mView;

    BaseModeRenderer(IPercentageChartView view) {
        mView = view;

        //DRAWING ORIENTATION
        orientation = ORIENTATION_CLOCKWISE;

        //START DRAWING ANGLE
        mStartAngle = DEFAULT_START_ANGLE;

        //BACKGROUND DRAW STATE
        mDrawBackground = this instanceof PieModeRenderer;

        //BACKGROUND COLOR
        mBackgroundColor = Color.BLACK.getValue();

        //PROGRESS
        mProgress = mTextProgress = 0;

        //PROGRESS COLOR
        mProgressColor = Color.RED.getValue();

        //GRADIENT COLORS
        mGradientType = -1;
        mGradientAngle = (int) mStartAngle;

        //PROGRESS ANIMATION DURATION
        mAnimDuration = DEFAULT_ANIMATION_DURATION;

        //PROGRESS ANIMATION INTERPOLATOR
        mCurveType = Animator.CurveType.INVALID;

        //TEXT COLOR
        mTextColor = Color.WHITE.getValue();

        //TEXT SIZE
//        mTextSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,
//                DEFAULT_TEXT_SP_SIZE,
//                mView.getViewContext().getResources().getDisplayMetrics());

        mTextSize = AttrHelper.fp2px(DEFAULT_TEXT_SP_SIZE, mView.getViewContext());
        //TEXT STYLE
        mTextStyle = NORMAL;

        //TEXT SHADOW
        mTextShadowColor = Color.TRANSPARENT.getValue();
        mTextShadowRadius = 0;
        mTextShadowDistX = 0;
        mTextShadowDistY = 0;

        //BACKGROUND OFFSET
        mBackgroundOffset = 0;

        mText = new Text(view.getViewContext());

    }

    BaseModeRenderer(IPercentageChartView view, AttrSet attrs) {
        mView = view;

        //DRAWING ORIENTATION
        orientation = ORIENTATION_CLOCKWISE;
        if (attrs.getAttr(PCV_ORIENTATION).isPresent()) {
            orientation = attrs.getAttr(PCV_ORIENTATION).get().getIntegerValue();
        }

        //START DRAWING ANGLE
        mStartAngle = DEFAULT_START_ANGLE;
        if (attrs.getAttr(PCV_START_ANGLE).isPresent()) {
            mStartAngle = attrs.getAttr(PCV_START_ANGLE).get().getIntegerValue();
        }
        if (mStartAngle < 0 || mStartAngle > 360) {
            mStartAngle = DEFAULT_START_ANGLE;
        }

        //BACKGROUND DRAW STATE
        mDrawBackground = (this instanceof PieModeRenderer || this instanceof FillModeRenderer);
        if (attrs.getAttr(PCV_DRAW_BACKGROUND).isPresent()) {
            mDrawBackground = attrs.getAttr(PCV_DRAW_BACKGROUND).get().getBoolValue();
        }

        //BACKGROUND COLOR
        mBackgroundColor = Color.BLACK.getValue();
        if (attrs.getAttr(PCV_BACKGROUND_COLOR).isPresent()) {
            mBackgroundColor = attrs.getAttr(PCV_BACKGROUND_COLOR).get().getIntegerValue();
        }

        //PROGRESS
        mProgress = 0;
        if (attrs.getAttr(PCV_PROGRESS).isPresent()) {
            mProgress = attrs.getAttr(PCV_PROGRESS).get().getIntegerValue();
        }
        if (mProgress < 0) {
            mProgress = 0;
        } else if (mProgress > 100) {
            mProgress = 100;
        }
        mTextProgress = (int) mProgress;

        //PROGRESS COLOR
        mProgressColor = Color.getIntColor("#F44336");
        if (attrs.getAttr(PCV_PROGRESS_COLOR).isPresent()) {
            mProgressColor = attrs.getAttr(PCV_PROGRESS_COLOR).get().getColorValue().getValue();
        }

        //GRADIENT COLORS
        initGradientColors(attrs);

        //PROGRESS ANIMATION DURATION
        mAnimDuration = DEFAULT_ANIMATION_DURATION;
        if (attrs.getAttr(PCV_ANIM_DURATION).isPresent()) {
            mAnimDuration = attrs.getAttr(PCV_ANIM_DURATION).get().getIntegerValue();
        }

        //PROGRESS ANIMATION INTERPOLATOR
        int interpolator = DEFAULT_ANIMATION_INTERPOLATOR;
        if (attrs.getAttr(PCV_ANIM_INTERPOLATOR).isPresent()) {
            interpolator = attrs.getAttr(PCV_ANIM_INTERPOLATOR).get().getDimensionValue();
        }
        mCurveType = interpolator;

        //TEXT COLOR
        mTextColor = Color.WHITE.getValue();
        if (attrs.getAttr(PCV_TEXT_COLOR).isPresent()) {
            mTextColor = attrs.getAttr(PCV_TEXT_COLOR).get().getColorValue().getValue();
        }

        //TEXT SIZE
        mTextSize = AttrHelper.fp2px(DEFAULT_TEXT_SP_SIZE, mView.getViewContext());
        if (attrs.getAttr(PCV_TEXT_SIZE).isPresent()) {
            mTextSize = attrs.getAttr(PCV_TEXT_SIZE).get().getDimensionValue();
        }

        //TEXT TYPEFACE
        String typeface = null;
        if (attrs.getAttr(PCV_TYPEFACE).isPresent()) {
            typeface = attrs.getAttr(PCV_TYPEFACE).get().getStringValue();
        }
        mTypeface = FontUtils.getFontBuilder(mView.getViewContext().getApplicationContext(), typeface);

        //TEXT STYLE
        mTextStyle = NORMAL;
        if (attrs.getAttr(PCV_TEXT_STYLE).isPresent()) {
            mTextStyle = attrs.getAttr(PCV_TEXT_STYLE).get().getIntegerValue();
        }
        if (mTextStyle > 0) {
            mTypeface = FontUtils.setTextStyle(mTypeface, mTextStyle);
        }

        //TEXT SHADOW
        mTextShadowColor = Color.TRANSPARENT.getValue();
        if (attrs.getAttr(PCV_TEXT_SHADOW_COLOR).isPresent()) {
            mTextShadowColor = attrs.getAttr(PCV_TEXT_SHADOW_COLOR).get().getColorValue().getValue();
        }

        if (mTextShadowColor != Color.TRANSPARENT.getValue()) {

            mTextShadowRadius = 0;
            if (attrs.getAttr("pcv_textShadowRadius").isPresent()) {
                mTextShadowRadius = attrs.getAttr("pcv_textShadowRadius").get().getFloatValue();
            }

            mTextShadowDistX = 0;
            if (attrs.getAttr("pcv_textShadowDistX").isPresent()) {
                mTextShadowDistX = attrs.getAttr("pcv_textShadowDistX").get().getFloatValue();
            }

            mTextShadowDistY = 0;
            if (attrs.getAttr("pcv_textShadowDistY").isPresent()) {
                mTextShadowDistY = attrs.getAttr("pcv_textShadowDistY").get().getFloatValue();
            }

        }

        //BACKGROUND OFFSET
        mBackgroundOffset = 0;
        if (attrs.getAttr(PCV_BACKGROUND_OFFSET).isPresent()) {
            mBackgroundOffset = attrs.getAttr(PCV_BACKGROUND_OFFSET).get().getIntegerValue();
        }

    }

    private void initGradientColors(AttrSet attrs) {
        //PROGRESS GRADIENT TYPE
        mGradientType = -1;
        if (attrs.getAttr(PCV_GRADIENT_TYPE).isPresent()) {
            mGradientType = attrs.getAttr(PCV_GRADIENT_TYPE).get().getIntegerValue();
        }
        if (mGradientType == -1) return;

        //ANGLE FOR LINEAR GRADIENT
        mGradientAngle = mStartAngle;
        if (attrs.getAttr(PCV_GRADIENT_ANGLE).isPresent()) {
            mGradientAngle = attrs.getAttr(PCV_GRADIENT_ANGLE).get().getFloatValue();
        }

        //PROGRESS GRADIENT COLORS
        String gradientColors = null;
        if (attrs.getAttr(PCV_GRADIENT_COLORS).isPresent()) {
            gradientColors = attrs.getAttr(PCV_GRADIENT_COLORS).get().getStringValue();
        }

        if (gradientColors != null) {
            String[] colors = gradientColors.split(",");
            mGradientColors = new int[colors.length];
//            try {
            for (int i = 0; i < colors.length; i++) {
//                    mGradientColors[i] = new Integer(Integer.valueOf(colors[i].trim()));
                mGradientColors[i] = Color.getIntColor(colors[i].trim());
            }
//            } catch (Exception e) {
//                throw new InflateException("pcv_gradientColors attribute contains invalid hex color values.");
//                e.printStackTrace();
//            }
        }

        //PROGRESS GRADIENT COLORS'S DISTRIBUTIONS
        String gradientDist = null;
        if (attrs.getAttr(PCV_GRADIENT_DISTRIBUTIONS).isPresent()) {
            gradientDist = attrs.getAttr(PCV_GRADIENT_DISTRIBUTIONS).get().getStringValue();
        }

        if (gradientDist != null) {
            String[] distributions = gradientDist.split(",");
            mGradientDistributions = new float[distributions.length];
//            try {
            for (int i = 0; i < distributions.length; i++) {
                mGradientDistributions[i] = Float.parseFloat(distributions[i].trim());
            }
//            } catch (Exception e) {
//                throw new InflateException("pcv_gradientDistributions attribute contains invalid values.");
//                e.printStackTrace();
//            }
        }
    }

    void setup() {
        mCircleBounds = new RectFloat();
        mBackgroundBounds = new RectFloat();
        mProvidedProgressColor = mProvidedBackgroundColor = mProvidedTextColor = -1;

        //BACKGROUND PAINT
        mBackgroundPaint = new Paint();
        mBackgroundPaint.setAntiAlias(true);
        mBackgroundPaint.setColor(new Color(mBackgroundColor));

        //PROGRESS PAINT
        mProgressPaint = new Paint();
        mProgressPaint.setAntiAlias(true);
        mProgressPaint.setColor(new Color(mProgressColor));

        //TEXT PAINT
        mTextPaint = new TextPaint();
        mTextPaint.setAntiAlias(true);
        mTextPaint.setTextAlign(TextAlignment.CENTER);
        mTextPaint.setTextSize((int) mTextSize);
        mTextPaint.setColor(new Color(mTextColor));

        if (mTypeface != null) {
            mTextPaint.setFont(mTypeface.build());
        }
        if (mTextShadowColor != Color.TRANSPARENT.getValue()) {
//            mTextPaint.setShadowLayer(mTextShadowRadius, mTextShadowDistX, mTextShadowDistY, mTextShadowColor);
            mTextPaintShadow = new TextPaint();
            mTextPaintShadow.set(mTextPaint);
            mTextPaintShadow.setColor(new Color(mTextShadowColor));
            mTextPaintShadow.setMaskFilter(new MaskFilter(mTextShadowRadius, MaskFilter.Blur.NORMAL));
        } else {
            mTextPaintShadow = null;
        }

        //TEXT LAYOUT
        defaultTextFormatter = progress -> (int) progress + "%";
//        mTextEditor = Editable.Factory.getInstance().newEditable(defaultTextFormatter.provideFormattedText(mTextProgress));
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
//            mTextLayout = DynamicLayout.Builder.obtain(mTextEditor, mTextPaint, Integer.MAX_VALUE)
//                    .setAlignment(Layout.Alignment.ALIGN_NORMAL)
//                    .setLineSpacing(0, 0)
//                    .setJustificationMode(Layout.JUSTIFICATION_MODE_NONE)
//                    .setBreakStrategy(Layout.HYPHENATION_FREQUENCY_NONE)
//                    .setIncludePad(false)
//                    .build();
//        } else {
//            mTextLayout = new DynamicLayout(mTextEditor,
//                    mTextPaint,
//                    Integer.MAX_VALUE,
//                    Layout.Alignment.ALIGN_NORMAL,
//                    0, 0,
//                    false);
//        }

        //ANIMATIONS
        mProgressAnimator = MyAnimatorValue.ofFloat((float) 0.0, mProgress);
        mProgressAnimator.setDuration(mAnimDuration);
        mProgressAnimator.setCurveType(mCurveType);
        mProgressAnimator.setValueUpdateListener((valueAnimator, v) -> {
            mProgress = (float) ((MyAnimatorValue) valueAnimator).getAnimatedValue();

            if (mProgress > 0 && mProgress <= 100) {
                mTextProgress = (int) mProgress;
            } else if (mProgress > 100) {
                mProgress = mTextProgress = 100;
            } else {
                mProgress = mTextProgress = 0;
            }

            updateDrawingAngles();
            updateText();

            mView.onProgressUpdated(mProgress);
            mView.postInvalidateOnAnimation();
        });
    }

    public void attach(IPercentageChartView view) {
        mView = view;
        setup();
    }

    //############################################################################################## INNER BEHAVIOR
    public abstract void measure(int w, int h, int paddingLeft, int paddingTop, int paddingRight, int paddingBottom);

    public abstract void draw(Canvas canvas);

    void drawText(Canvas canvas) {
        canvas.save();
//        canvas.translate(mCircleBounds.getCenter().getPointX(), mCircleBounds.getCenter().getPointY() - (mTextLayout.getHeight() >> 1));
//        mTextLayout.draw(canvas);
        if (mTextPaintShadow != null) {
            canvas.drawText(mTextPaintShadow, mTextProgress + "%", mCircleBounds.getCenter().getPointX() + 10 + mTextShadowDistX, mCircleBounds.getCenter().getPointY() + 20 + mTextShadowDistY);
        }
        canvas.drawText(mTextPaint, mTextProgress + "%", mCircleBounds.getCenter().getPointX() + 10, mCircleBounds.getCenter().getPointY() + 20);
        canvas.restore();
    }

    public void destroy() {
        if (mProgressAnimator != null) {
            if (mProgressAnimator.isRunning()) {
                mProgressAnimator.cancel();
            }
            mProgressAnimator.setValueUpdateListener(null);
        }

        if (mProgressColorAnimator != null) {
            if (mProgressColorAnimator.isRunning()) {
                mProgressColorAnimator.cancel();
            }
            mProgressColorAnimator.setValueUpdateListener(null);
        }

        if (mBackgroundColorAnimator != null) {
            if (mBackgroundColorAnimator.isRunning()) {
                mBackgroundColorAnimator.cancel();
            }
            mBackgroundColorAnimator.setValueUpdateListener(null);
        }

        if (mTextColorAnimator != null) {
            if (mTextColorAnimator.isRunning()) {
                mTextColorAnimator.cancel();
            }
            mTextColorAnimator.setValueUpdateListener(null);
        }

        mProgressAnimator = mProgressColorAnimator = mBackgroundColorAnimator = mTextColorAnimator = null;
        mCircleBounds = mBackgroundBounds = null;
        mBackgroundPaint = mProgressPaint = mTextPaint = null;
        mGradientShader = null;
        mAdaptiveColorProvider = null;
        defaultTextFormatter = mProvidedTextFormatter = null;
    }

    void updateText() {
        if (mText != null) {
            CharSequence text = (mProvidedTextFormatter != null) ?
                    mProvidedTextFormatter.provideFormattedText(mTextProgress) :
                    defaultTextFormatter.provideFormattedText(mTextProgress);
            mText.setText(text.toString());
        }
    }

    abstract void updateDrawingAngles();

    void updateProvidedColors(float progress) {
        if (mAdaptiveColorProvider == null) return;
        int providedProgressColor = mAdaptiveColorProvider.provideProgressColor(progress);

        if (providedProgressColor != -1 && providedProgressColor != mProvidedProgressColor && mGradientType == -1) {
            mProvidedProgressColor = providedProgressColor;
            mProgressPaint.setColor(new Color(mProvidedProgressColor));
        }


        int providedBackgroundColor = mAdaptiveColorProvider.provideBackgroundColor(progress);

        if (providedBackgroundColor != -1 && providedBackgroundColor != mProvidedBackgroundColor) {
            mProvidedBackgroundColor = providedBackgroundColor;
            mBackgroundPaint.setColor(new Color(mProvidedBackgroundColor));
        }


        int providedTextColor = mAdaptiveColorProvider.provideTextColor(progress);

        if (providedTextColor != -1 && providedTextColor != mProvidedTextColor) {
            mProvidedTextColor = providedTextColor;
            mTextPaint.setColor(new Color(mProvidedTextColor));
        }
    }

    void setupColorAnimations() {
        if (mProgressColorAnimator == null) {
            mProgressColorAnimator = MyAnimatorValue.ofObject(new ArgbEvaluator(), mProgressColor, mProvidedProgressColor);
            mProgressColorAnimator.setValueUpdateListener((animation, v) -> {
                mProvidedProgressColor = (int) ((MyAnimatorValue) animation).getAnimatedValue();
                mProgressPaint.setColor(new Color(mProvidedProgressColor));
            });
            mProgressColorAnimator.setDuration(mAnimDuration);
        }

        if (mBackgroundColorAnimator == null) {
            mBackgroundColorAnimator = MyAnimatorValue.ofObject(new ArgbEvaluator(), mBackgroundColor, mProvidedBackgroundColor);
            mBackgroundColorAnimator.setValueUpdateListener((animation, v) -> {
                mProvidedBackgroundColor = (int) ((MyAnimatorValue) animation).getAnimatedValue();
                mBackgroundPaint.setColor(new Color(mProvidedBackgroundColor));
            });
            mBackgroundColorAnimator.setDuration(mAnimDuration);
        }

        if (mTextColorAnimator == null) {
            mTextColorAnimator = MyAnimatorValue.ofObject(new ArgbEvaluator(), mTextColor, mProvidedTextColor);
            mTextColorAnimator.setValueUpdateListener((animation, v) -> {
                mProvidedTextColor = (int) ((MyAnimatorValue) animation).getAnimatedValue();
                mTextPaint.setColor(new Color(mProvidedTextColor));
            });
            mTextColorAnimator.setDuration(mAnimDuration);
        }
    }

    void updateAnimations(float progress) {
        mProgressAnimator.setFloatValues(mProgress, progress);
        mProgressAnimator.start();

        if (mAdaptiveColorProvider == null) return;

        int providedProgressColor = mAdaptiveColorProvider.provideProgressColor(progress);
        if (providedProgressColor != -1 && providedProgressColor != mProvidedProgressColor && mGradientType == -1) {
            mProvidedProgressColor = providedProgressColor;
            mProgressPaint.setColor(new Color(mProvidedProgressColor));
        }

        int providedBackgroundColor = mAdaptiveColorProvider.provideBackgroundColor(progress);
        if (providedBackgroundColor != -1 && providedBackgroundColor != mProvidedBackgroundColor) {
            mProvidedBackgroundColor = providedBackgroundColor;
            mBackgroundPaint.setColor(new Color(mProvidedBackgroundColor));
        }

        int providedTextColor = mAdaptiveColorProvider.provideTextColor(progress);
        if (providedTextColor != -1 && providedTextColor != mProvidedTextColor) {
            mProvidedTextColor = providedTextColor;
            mTextPaint.setColor(new Color(mProvidedTextColor));
        }
    }

    void cancelAnimations() {
        if (mProgressAnimator.isRunning()) {
            mProgressAnimator.cancel();
        }

        if (mProgressColorAnimator != null && mProgressColorAnimator.isRunning()) {
            mProgressColorAnimator.cancel();
        }

        if (mBackgroundColorAnimator != null && mBackgroundColorAnimator.isRunning()) {
            mBackgroundColorAnimator.cancel();
        }

        if (mTextColorAnimator != null && mTextColorAnimator.isRunning()) {
            mTextColorAnimator.cancel();
        }
    }

    abstract void setupGradientColors(RectFloat bounds);

    abstract void updateGradientAngle(float angle);

    //############################################################################################## MODIFIERS
    public abstract void setAdaptiveColorProvider(AdaptiveColorProvider adaptiveColorProvider);

    public void setTextFormatter(ProgressTextFormatter textFormatter) {
        this.mProvidedTextFormatter = textFormatter;
        updateText();
        mView.postInvalidate();
    }

    //PROGRESS
    public float getProgress() {
        return mProgress;
    }

    public void setProgress(float progress, boolean animate) {
        if (this.mProgress == progress) return;

        cancelAnimations();

        if (!animate) {
            this.mProgress = progress;
            this.mTextProgress = (int) progress;

            updateProvidedColors(progress);
            updateDrawingAngles();
            updateText();

            mView.onProgressUpdated(mProgress);
            mView.postInvalidate();
            return;
        }

        updateAnimations(progress);
    }

    //DRAW BACKGROUND STATE
    public boolean isDrawBackgroundEnabled() {
        return mDrawBackground;
    }

    public void setDrawBackgroundEnabled(boolean drawBackground) {
        if (this.mDrawBackground == drawBackground) return;
        this.mDrawBackground = drawBackground;
    }

    //START ANGLE
    public float getStartAngle() {
        return mStartAngle;
    }

    public abstract void setStartAngle(float startAngle);

    //BACKGROUND COLOR
    public int getBackgroundColor() {
        if (!mDrawBackground) return -1;
        return mBackgroundColor;
    }

    public void setBackgroundColor(int backgroundColor) {
        if ((mAdaptiveColorProvider != null && mAdaptiveColorProvider.provideBackgroundColor(mProgress) != -1) || this.mBackgroundColor == backgroundColor)
            return;
        this.mBackgroundColor = backgroundColor;
        if (!mDrawBackground) return;
        mBackgroundPaint.setColor(new Color(mBackgroundColor));
    }

    //PROGRESS COLOR
    public int getProgressColor() {
        return mProgressColor;
    }

    public void setProgressColor(int progressColor) {
        if ((mAdaptiveColorProvider != null && mAdaptiveColorProvider.provideProgressColor(mProgress) != -1) || this.mProgressColor == progressColor)
            return;

        this.mProgressColor = progressColor;
        mProgressPaint.setColor(new Color(progressColor));
    }

    //GRADIENT COLORS
    public int getGradientType() {
        return mGradientType;
    }

    public void setGradientColors(int type, int[] colors, float[] positions, float angle) {
        mGradientType = type;
        mGradientColors = colors;
        mGradientDistributions = positions;
        setupGradientColors(mCircleBounds);
        if (mGradientType == GRADIENT_LINEAR && mGradientAngle != angle) {
            mGradientAngle = angle;
            updateGradientAngle(mGradientAngle);
        }
    }

    public void setGradientColorsInternal(int type, int[] colors, float[] positions, float angle) {
        mGradientType = type;
        mGradientColors = colors;
        mGradientDistributions = positions;
        if (mGradientType == GRADIENT_LINEAR && mGradientAngle != angle) {
            mGradientAngle = angle;
        }
    }

    public float getGradientAngle() {
        return mGradientAngle;
    }

    public int[] getGradientColors() {
        return mGradientColors;
    }

    public float[] getGradientDistributions() {
        return mGradientDistributions;
    }

    //ANIMATION DURATION
    public int getAnimationDuration() {
        return mAnimDuration;
    }

    public void setAnimationDuration(int duration) {
        if (this.mAnimDuration == duration) return;
        mAnimDuration = duration;
        mProgressAnimator.setDuration(mAnimDuration);
        if (mProgressColorAnimator != null) {
            mProgressColorAnimator.setDuration(mAnimDuration);
        }
        if (mBackgroundColorAnimator != null) {
            mBackgroundColorAnimator.setDuration(mAnimDuration);
        }
        if (mTextColorAnimator != null) {
            mTextColorAnimator.setDuration(mAnimDuration);
        }
        if (mBgBarColorAnimator != null) {
            mBgBarColorAnimator.setDuration(mAnimDuration);
        }
    }

    //ANIMATION INTERPOLATOR
    public int getAnimationInterpolator() {
        return mProgressAnimator.getCurveType();
    }

    public void setAnimationInterpolator(int curveType) {
        mProgressAnimator.setCurveType(curveType);
    }

    //TEXT COLOR
    public int getTextColor() {
        return mTextColor;
    }

    public void setTextColor(int textColor) {
        if ((mAdaptiveColorProvider != null && mAdaptiveColorProvider.provideTextColor(mProgress) != -1) || this.mTextColor == textColor)
            return;
        this.mTextColor = textColor;
        mTextPaint.setColor(new Color(textColor));
    }

    //TEXT SIZE
    public float getTextSize() {
        return mTextSize;
    }

    public void setTextSize(float textSize) {
        if (this.mTextSize == textSize) return;
        this.mTextSize = textSize;
        mTextPaint.setTextSize((int) textSize);
        updateText();
    }

    //TEXT Font
    public Font.Builder getTypeface() {
        return mTypeface;
    }

    public void setTypeface(Font.Builder typeface) {
        if (this.mTypeface != null && this.mTypeface.equals(typeface)) return;
        this.mTypeface = mTextStyle > 0 ?
                FontUtils.setTextStyle(typeface, mTextStyle) :
                typeface;
        mTextPaint.setFont(mTypeface.build());
        updateText();
    }

    //TEXT STYLE
    public int getTextStyle() {
        return mTextStyle;
    }

    public void setTextStyle(int mTextStyle) {
        if (this.mTextStyle == mTextStyle) return;
        this.mTextStyle = mTextStyle;
        mTypeface = FontUtils.setTextStyle(mTypeface, mTextStyle);

        mTextPaint.setFont(mTypeface.build());
        updateText();
    }

    //TEXT SHADOW
    public int getTextShadowColor() {
        return mTextShadowColor;
    }

    public float getTextShadowRadius() {
        return mTextShadowRadius;
    }

    public float getTextShadowDistY() {
        return mTextShadowDistY;
    }

    public float getTextShadowDistX() {
        return mTextShadowDistX;
    }

    public void setTextShadow(int shadowColor, float shadowRadius, float shadowDistX, float shadowDistY) {
        if (this.mTextShadowColor == shadowColor
                && this.mTextShadowRadius == shadowRadius
                && this.mTextShadowDistX == shadowDistX
                && this.mTextShadowDistY == shadowDistY) return;
        this.mTextShadowColor = shadowColor;
        this.mTextShadowRadius = shadowRadius;
        this.mTextShadowDistX = shadowDistX;
        this.mTextShadowDistY = shadowDistY;

//        mTextPaint.setShadowLayer(mTextShadowRadius, mTextShadowDistX, mTextShadowDistY, mTextShadowColor);
        if (shadowColor != 0) {
            mTextPaintShadow = new TextPaint();
            mTextPaintShadow.set(mTextPaint);
            mTextPaintShadow.setColor(new Color(mTextShadowColor));
            mTextPaintShadow.setMaskFilter(new MaskFilter(mTextShadowRadius, MaskFilter.Blur.NORMAL));
        } else {
            mTextPaintShadow = null;
        }
        updateText();
    }

}