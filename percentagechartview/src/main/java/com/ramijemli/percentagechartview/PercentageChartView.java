/*
 * Copyright 2018 Rami Jemli
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ramijemli.percentagechartview;

import com.ramijemli.percentagechartview.annotation.*;
import com.ramijemli.percentagechartview.callback.AdaptiveColorProvider;
import com.ramijemli.percentagechartview.callback.OnProgressChangeListener;
import com.ramijemli.percentagechartview.callback.ProgressTextFormatter;
import com.ramijemli.percentagechartview.renderer.*;
import com.ramijemli.percentagechartview.utils.MyAttrsUtils;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.text.Font;
import ohos.app.Context;

import static com.ramijemli.percentagechartview.utils.MyAttrsUtils.*;


@SuppressWarnings({"unused", "UnusedReturnValue"})
public class PercentageChartView extends Component implements IPercentageChartView, Component.DrawTask {

    private static final String STATE_SUPER_INSTANCE = "PercentageChartView.STATE_SUPER_INSTANCE";
    private static final String STATE_MODE = "PercentageChartView.STATE_MODE";
    private static final String STATE_ORIENTATION = "PercentageChartView.STATE_ORIENTATION";
    private static final String STATE_START_ANGLE = "PercentageChartView.STATE_START_ANGLE";
    private static final String STATE_DURATION = "PercentageChartView.STATE_DURATION";

    private static final String STATE_PROGRESS = "PercentageChartView.STATE_PROGRESS";
    private static final String STATE_PG_COLOR = "PercentageChartView.STATE_PG_COLOR";

    private static final String STATE_DRAW_BG = "PercentageChartView.STATE_DRAW_BG";
    private static final String STATE_BG_COLOR = "PercentageChartView.STATE_BG_COLOR";
    private static final String STATE_BG_OFFSET = "PercentageChartView.STATE_BG_OFFSET";

    private static final String STATE_TXT_COLOR = "PercentageChartView.STATE_TXT_COLOR";
    private static final String STATE_TXT_SIZE = "PercentageChartView.STATE_TXT_SIZE";
    private static final String STATE_TXT_SHA_COLOR = "PercentageChartView.STATE_TXT_SHD_COLOR";
    private static final String STATE_TXT_SHA_RADIUS = "PercentageChartView.STATE_TXT_SHA_RADIUS";
    private static final String STATE_TXT_SHA_DIST_X = "PercentageChartView.STATE_TXT_SHA_DIST_X";
    private static final String STATE_TXT_SHA_DIST_Y = "PercentageChartView.STATE_TXT_SHA_DIST_Y";

    private static final String STATE_PG_BAR_THICKNESS = "PercentageChartView.STATE_PG_BAR_THICKNESS";
    private static final String STATE_PG_BAR_STYLE = "PercentageChartView.STATE_PG_BAR_STYLE";

    private static final String STATE_DRAW_BG_BAR = "PercentageChartView.STATE_DRAW_BG_BAR";
    private static final String STATE_BG_BAR_COLOR = "PercentageChartView.STATE_BG_BAR_COLOR";
    private static final String STATE_BG_BAR_THICKNESS = "PercentageChartView.STATE_BG_BAR_THICKNESS";

    private static final String STATE_GRADIENT_TYPE = "PercentageChartView.STATE_GRADIENT_TYPE";
    private static final String STATE_GRADIENT_ANGLE = "PercentageChartView.STATE_GRADIENT_ANGLE";
    private static final String STATE_GRADIENT_COLORS = "PercentageChartView.STATE_GRADIENT_COLORS";
    private static final String STATE_GRADIENT_POSITIONS = "PercentageChartView.STATE_GRADIENT_POSITIONS";

    private BaseModeRenderer renderer;

    @ChartMode
    private int mode;


    private OnProgressChangeListener onProgressChangeListener;

    public PercentageChartView(Context context) {
        super(context);
        init(context, null);
    }

    public PercentageChartView(Context context, AttrSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public PercentageChartView(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public PercentageChartView(Context context, AttrSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttrSet attributeSet) {
        if (attributeSet != null) {
            AttrSet attrs = attributeSet;

            //CHART MODE (DEFAULT PIE MODE)
            mode = MODE_PIE;
            if (attrs.getAttr("pcv_mode").isPresent()) {
                mode = MyAttrsUtils.pcv_mode.get(attrs.getAttr("pcv_mode").get().getStringValue());
            }
            switch (mode) {
                case MODE_RING:
                    renderer = new RingModeRenderer(this, attrs);
                    break;
                case MODE_FILL:
                    renderer = new FillModeRenderer(this, attrs);
                    break;
                case MODE_PIE:
                    renderer = new PieModeRenderer(this, attrs);
                    break;
            }
        } else {
            mode = MODE_PIE;
            renderer = new PieModeRenderer(this);
        }

        addDrawTask(this, BETWEEN_BACKGROUND_AND_CONTENT);
        setEstimateSizeListener(this::onMeasure);
    }

    //##############################################################################################   BEHAVIOR

    public boolean onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int w = MeasureSpec.getSize(widthMeasureSpec);
        int h = MeasureSpec.getSize(heightMeasureSpec);
        if (renderer != null)
            renderer.measure(w, h, getPaddingLeft(), getPaddingTop(), getPaddingRight(), getPaddingBottom());
        setEstimatedSize(widthMeasureSpec, heightMeasureSpec);
        return true;
    }

    @Override
    public void release() {
        super.release();
        renderer.destroy();
        renderer = null;

        if (onProgressChangeListener != null) {
            onProgressChangeListener = null;
        }
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        renderer.draw(canvas);
    }

    // 不支持
//    @Override
//    protected Parcelable onSaveInstanceState() {
//        Bundle bundle = new Bundle();
//        bundle.putParcelable(STATE_SUPER_INSTANCE, super.onSaveInstanceState());
//
//        bundle.putInt(STATE_MODE, mode);
//        if (renderer instanceof OrientationBasedMode) {
//            bundle.putInt(STATE_ORIENTATION, ((OrientationBasedMode) renderer).getOrientation());
//        }
//        bundle.putFloat(STATE_START_ANGLE, renderer.getStartAngle());
//        bundle.putInt(STATE_DURATION, renderer.getAnimationDuration());
//
//        bundle.putFloat(STATE_PROGRESS, renderer.getProgress());
//        bundle.putInt(STATE_PG_COLOR, renderer.getProgressColor());
//
//        bundle.putBoolean(STATE_DRAW_BG, renderer.isDrawBackgroundEnabled());
//        bundle.putInt(STATE_BG_COLOR, renderer.getBackgroundColor());
//        if (renderer instanceof OffsetEnabledMode) {
//            bundle.putInt(STATE_BG_OFFSET, ((OffsetEnabledMode) renderer).getBackgroundOffset());
//        }
//
//        bundle.putInt(STATE_TXT_COLOR, renderer.getTextColor());
//        bundle.putFloat(STATE_TXT_SIZE, renderer.getTextSize());
//        bundle.putInt(STATE_TXT_SHA_COLOR, renderer.getTextShadowColor());
//        bundle.putFloat(STATE_TXT_SHA_RADIUS, renderer.getTextShadowRadius());
//        bundle.putFloat(STATE_TXT_SHA_DIST_X, renderer.getTextShadowDistX());
//        bundle.putFloat(STATE_TXT_SHA_DIST_Y, renderer.getTextShadowDistY());
//
//        if (renderer instanceof RingModeRenderer) {
//            bundle.putFloat(STATE_PG_BAR_THICKNESS, ((RingModeRenderer) renderer).getProgressBarThickness());
//            bundle.putInt(STATE_PG_BAR_STYLE, ((RingModeRenderer) renderer).getProgressBarStyle());
//            bundle.putBoolean(STATE_DRAW_BG_BAR, ((RingModeRenderer) renderer).isDrawBackgroundBarEnabled());
//            bundle.putInt(STATE_BG_BAR_COLOR, ((RingModeRenderer) renderer).getBackgroundBarColor());
//            bundle.putFloat(STATE_BG_BAR_THICKNESS, ((RingModeRenderer) renderer).getBackgroundBarThickness());
//        }
//
//        if (renderer.getGradientType() != -1) {
//            bundle.putInt(STATE_GRADIENT_TYPE, renderer.getGradientType());
//            bundle.putFloat(STATE_GRADIENT_ANGLE, renderer.getGradientAngle());
//            bundle.putIntArray(STATE_GRADIENT_COLORS, renderer.getGradientColors());
//            bundle.putFloatArray(STATE_GRADIENT_POSITIONS, renderer.getGradientDistributions());
//        }
//
//        return bundle;
//    }
//
//    @Override
//    protected void onRestoreInstanceState(Parcelable state) {
//        if (state instanceof Bundle) {
//            Bundle bundle = (Bundle) state;
//            mode = bundle.getInt(STATE_MODE);
//            switch (mode) {
//                case MODE_RING:
//                    renderer = new RingModeRenderer(this);
//                    ((RingModeRenderer) renderer).setProgressBarThickness(bundle.getFloat(STATE_PG_BAR_THICKNESS));
//                    ((RingModeRenderer) renderer).setProgressBarStyle(bundle.getInt(STATE_PG_BAR_STYLE));
//                    ((RingModeRenderer) renderer).setDrawBackgroundBarEnabled(bundle.getBoolean(STATE_DRAW_BG_BAR));
//                    ((RingModeRenderer) renderer).setBackgroundBarColor(bundle.getInt(STATE_BG_BAR_COLOR));
//                    ((RingModeRenderer) renderer).setBackgroundBarThickness(bundle.getFloat(STATE_BG_BAR_THICKNESS));
//                    break;
//                case MODE_FILL:
//                    renderer = new FillModeRenderer(this);
//                    break;
//
//                default:
//                case MODE_PIE:
//                    renderer = new PieModeRenderer(this);
//                    break;
//            }
//
//            if (renderer instanceof OrientationBasedMode) {
//                ((OrientationBasedMode) renderer).setOrientation(bundle.getInt(STATE_ORIENTATION));
//            }
//            renderer.setStartAngle(bundle.getFloat(STATE_START_ANGLE));
//            renderer.setAnimationDuration(bundle.getInt(STATE_DURATION));
//
//            renderer.setProgress(bundle.getFloat(STATE_PROGRESS), false);
//            renderer.setProgressColor(bundle.getInt(STATE_PG_COLOR));
//
//            renderer.setDrawBackgroundEnabled(bundle.getBoolean(STATE_DRAW_BG));
//            renderer.setBackgroundColor(bundle.getInt(STATE_BG_COLOR));
//            if (renderer instanceof OffsetEnabledMode) {
//                ((OffsetEnabledMode) renderer).setBackgroundOffset(bundle.getInt(STATE_BG_OFFSET));
//            }
//
//            renderer.setTextColor(bundle.getInt(STATE_TXT_COLOR));
//            renderer.setTextSize(bundle.getFloat(STATE_TXT_SIZE));
//            renderer.setTextShadow(bundle.getInt(STATE_TXT_SHA_COLOR),
//                    bundle.getFloat(STATE_TXT_SHA_RADIUS),
//                    bundle.getFloat(STATE_TXT_SHA_DIST_X),
//                    bundle.getFloat(STATE_TXT_SHA_DIST_Y));
//
//            if (bundle.getInt(STATE_GRADIENT_TYPE, -1) != -1) {
//                renderer.setGradientColorsInternal(bundle.getInt(STATE_GRADIENT_TYPE),
//                        bundle.getIntArray(STATE_GRADIENT_COLORS),
//                        bundle.getFloatArray(STATE_GRADIENT_POSITIONS),
//                        bundle.getFloat(STATE_GRADIENT_ANGLE));
//            }
//            super.onRestoreInstanceState(bundle.getParcelable(STATE_SUPER_INSTANCE));
//            return;
//        }
//
//        super.onRestoreInstanceState(state);
//    }

    //RENDERER CALLBACKS
    @Override
    public Context getViewContext() {
        return getContext();
    }

    @Override
    public void postInvalidate() {
        invalidate();
    }

    @Override
    public void postInvalidateOnAnimation() {
        invalidate();
    }

    @Override
    public boolean isInEditMode() {
        return false;
    }

    @Override
    public void onProgressUpdated(float progress) {
        if (onProgressChangeListener != null)
            onProgressChangeListener.onProgressChanged(progress);
    }

    //##############################################################################################   STYLE MODIFIERS

    /**
     * Gets the percentage chart view mode.
     *
     * @return the percentage chart view mode
     */
    @ChartMode
    public int getMode_() {
        return mode;
    }

    /**
     * Gets the current drawing orientation.
     *
     * @return the current drawing orientation
     */
    @ProgressOrientation
    public int getOrientation() {
        if (!(renderer instanceof OrientationBasedMode))
            return MyAttrsUtils.INVALID_ORIENTATION;
        return ((OrientationBasedMode) renderer).getOrientation();
    }

    /**
     * Sets the circular drawing direction. Default orientation is ORIENTATION_CLOCKWISE.
     *
     * @param orientation non-negative orientation constant.
     */
    public void setOrientation(@ProgressOrientation int orientation) {
        orientation(orientation);
        invalidate();
    }

    /**
     * Gets the current circular drawing's start angle.
     *
     * @return the current circular drawing's start angle
     */
    public float getStartAngle() {
        return renderer.getStartAngle();
    }

    /**
     * Sets the current circular drawing's start angle in degrees. Default start angle is0.
     *
     * @param startAngle A positive start angle value that is less or equal to 360.
     */
    public void setStartAngle(float startAngle) {
        startAngle(startAngle);
        this.invalidate();
    }

    /**
     * Gets whether drawing background has been enabled.
     *
     * @return whether drawing background has been enabled
     */
    public boolean isDrawBackgroundEnabled() {
        return renderer.isDrawBackgroundEnabled();
    }

    /**
     * Sets whether background should be drawn.
     *
     * @param enabled True if background have to be drawn, false otherwise.
     */
    public void setDrawBackgroundEnabled(boolean enabled) {
        drawBackgroundEnabled(enabled);
        this.invalidate();
    }

    /**
     * Gets the circular background color for this view.
     *
     * @return the color of the circular background
     */
    public int getBackgroundColor() {
        return renderer.getBackgroundColor();
    }

    /**
     * Sets the circular background color for this view.
     *
     * @param color the color of the circular background
     */
    public void setBackgroundColor(int color) {
        backgroundColor(color);
        this.invalidate();
    }

    /**
     * Gets the current progress.
     *
     * @return the current progress
     */
    public float getProgress() {
        return renderer.getProgress();
    }

    /**
     * Sets a new progress value. Passing true in animate will cause an animated progress update.
     *
     * @param progress New progress float value to set.
     * @param animate  Animation boolean value to set whether to animate progress change or not.
     * @throws IllegalArgumentException if the given progress is negative, or, less or equal to 100.
     */
    public void setProgress(float progress, boolean animate) {
        if (progress < 0 || progress > 100) {
            throw new IllegalArgumentException("Progress value must be positive and less or equal to 100.");
        }

        renderer.setProgress(progress, animate);
    }

    /**
     * Gets the progress/progress bar color for this view.
     *
     * @return the progress/progress bar color.
     */
    public int getProgressColor() {
        return renderer.getProgressColor();
    }

    /**
     * Sets the progress/progress bar color for this view.
     *
     * @param color the color of the progress/progress bar
     */
    public void setProgressColor(int color) {
        progressColor(color);
        this.invalidate();
    }


    /**
     * Gets progress gradient type.
     *
     * @return Gets progress gradient type.
     */
    @GradientTypes
    public int getGradientType() {
        return renderer.getGradientType();
    }


    /**
     * Sets progress gradient colors.
     *
     * @param type      The gradient type which is a GradientTypes constant
     * @param colors    The colors to be distributed.
     *                  There must be at least 2 colors in the array.
     * @param positions May be NULL. The relative position of
     *                  each corresponding color in the colors array, beginning
     *                  with 0 and ending with 1.0. If the values are not
     *                  monotonic, the drawing may produce unexpected results.
     *                  If positions is NULL, then the colors are automatically
     *                  spaced evenly.
     * @param angle     Defines the direction for linear gradient type.
     */
    public void setGradientColors(@GradientTypes int type, int[] colors, float[] positions, float angle) {
        gradientColors(type, colors, positions, angle);
        this.invalidate();
    }

    /**
     * Gets the duration of the progress change's animation.
     *
     * @return the duration of the progress change's animation
     */
    public int getAnimationDuration() {
        return renderer.getAnimationDuration();
    }

    /**
     * Sets the duration of the progress change's animation.
     *
     * @param duration non-negative duration value.
     */
    public void setAnimationDuration(int duration) {
        animationDuration(duration);
    }

    /**
     * Gets the interpolator of the progress change's animation.
     *
     * @return the interpolator of the progress change's animation
     */
    public int getAnimationInterpolator() {
        return renderer.getAnimationInterpolator();
    }

    /**
     * Sets the interpolator of the progress change's animation.
     *
     * @param curveType Specifies the interpolator type for the animator.
     */
    @SuppressWarnings("ConstantConditions")
    public void setAnimationInterpolator(int curveType) {
        animationInterpolator(curveType);
    }

    /**
     * Gets the text color.
     *
     * @return the text color
     */
    public int getTextColor() {
        return renderer.getTextColor();
    }

    /**
     * Sets the text color for this view.
     *
     * @param color the text color
     */
    public void setTextColor(int color) {
        textColor(color);
        this.invalidate();
    }

    /**
     * Gets the text size.
     *
     * @return the text size
     */
    public float getTextSize() {
        return renderer.getTextSize();
    }

    /**
     * Sets the text size.
     *
     * @param size the text size
     */
    public void setTextSize(float size) {
        textSize(size);
        this.invalidate();
    }

    /**
     * Gets the text font.
     *
     * @return the text Font
     */
    public Font.Builder getTypeface() {
        return renderer.getTypeface();
    }

    /**
     * Sets the text font.
     *
     * @param typeface the text font as a Font instance
     */
    @SuppressWarnings("ConstantConditions")
    public void setTypeface(Font.Builder typeface) {
        typeface(typeface);
        this.invalidate();
    }

    /**
     * Gets the text style.
     *
     * @return the text style
     */
    @TextStyle
    public int getTextStyle() {
        return renderer.getTextStyle();
    }

    /**
     * Sets the text style.
     *
     * @param style the text style.
     */
    public void setTextStyle(@TextStyle int style) {
        textStyle(style);
        this.invalidate();
    }

    /**
     * Gets the text shadow color.
     *
     * @return the text shadow color
     */
    public int getTextShadowColor() {
        return renderer.getTextShadowColor();
    }

    /**
     * Gets the text shadow radius.
     *
     * @return the text shadow radius
     */
    public float getTextShadowRadius() {
        return renderer.getTextShadowRadius();
    }

    /**
     * Gets the text shadow y-axis distance.
     *
     * @return the text shadow y-axis distance
     */
    public float getTextShadowDistY() {
        return renderer.getTextShadowDistY();
    }

    /**
     * Gets the text shadow x-axis distance.
     *
     * @return the text shadow x-axis distance
     */
    public float getTextShadowDistX() {
        return renderer.getTextShadowDistX();
    }

    /**
     * Sets the text shadow. Passing zeros will remove the shadow.
     *
     * @param shadowColor  text shadow color value.
     * @param shadowRadius text shadow radius.
     * @param shadowDistX  text shadow y-axis distance.
     * @param shadowDistY  text shadow x-axis distance.
     */
    public void setTextShadow(int shadowColor, float shadowRadius, float shadowDistX, float shadowDistY) {
        textShadow(shadowColor, shadowRadius, shadowDistX, shadowDistY);
        this.invalidate();
    }

    /**
     * Gets the offset of the circular background.
     *
     * @return the offset of the circular background.-1 if chart mode is not set to pie.
     */
    public float getBackgroundOffset() {
        if (!(renderer instanceof OffsetEnabledMode)) return -1;
        return ((OffsetEnabledMode) renderer).getBackgroundOffset();
    }

    /**
     * Sets the offset of the circular background. Works only if chart mode is set to pie.
     *
     * @param offset A positive offset value.
     */
    public void setBackgroundOffset(int offset) {
        backgroundOffset(offset);
        this.invalidate();
    }

    /**
     * Gets whether drawing the background bar has been enabled.
     *
     * @return whether drawing the background bar has been enabled
     */
    public boolean isDrawBackgroundBarEnabled() {
        if (!(renderer instanceof RingModeRenderer)) return false;
        return ((RingModeRenderer) renderer).isDrawBackgroundBarEnabled();
    }

    /**
     * Sets whether background bar should be drawn.
     *
     * @param enabled True if background bar have to be drawn, false otherwise.
     */
    public void setDrawBackgroundBarEnabled(boolean enabled) {
        drawBackgroundBarEnabled(enabled);
        this.invalidate();
    }

    /**
     * Gets the background bar color.
     *
     * @return the background bar color. -1 if chart mode is not set to ring.
     */
    public int getBackgroundBarColor() {
        if (!(renderer instanceof RingModeRenderer)) return -1;
        return ((RingModeRenderer) renderer).getBackgroundBarColor();
    }

    /**
     * Sets the background bar color.
     *
     * @param color the background bar color
     */
    public void setBackgroundBarColor(int color) {
        backgroundBarColor(color);
        this.invalidate();
    }

    /**
     * Gets the background bar thickness in pixels.
     *
     * @return the background bar thickness in pixels. -1 if chart mode is not set to ring.
     */
    public float getBackgroundBarThickness() {
        if (!(renderer instanceof RingModeRenderer)) return -1;
        return ((RingModeRenderer) renderer).getBackgroundBarThickness();
    }

    /**
     * Sets the background bar thickness in pixels. Works only if chart mode is set to ring.
     *
     * @param thickness non-negative thickness value in pixels.
     */
    public void setBackgroundBarThickness(float thickness) {
        backgroundBarThickness(thickness);
        this.invalidate();
    }

    /**
     * Gets the progress bar thickness in pixels.
     *
     * @return the progress bar thickness in pixels. -1 if chart mode is not set to ring.
     */
    public float getProgressBarThickness() {
        if (!(renderer instanceof RingModeRenderer)) return -1;
        return ((RingModeRenderer) renderer).getProgressBarThickness();
    }

    /**
     * Sets the progress bar thickness in pixels. Works only if chart mode is set to ring.
     *
     * @param thickness non-negative thickness value in pixels.
     */
    public void setProgressBarThickness(float thickness) {
        progressBarThickness(thickness);
        this.invalidate();
    }

    /**
     * Gets the progress bar stroke style.
     *
     * @return the progress bar stroke style. -1 if chart mode is not set to ring.
     */
    public int getProgressBarStyle() {
        if (!(renderer instanceof RingModeRenderer)) return -1;
        return ((RingModeRenderer) renderer).getProgressBarStyle();
    }

    /**
     * Sets the progress bar stroke style. Works only if chart mode is set to ring.
     *
     * @param style Progress bar stroke style as a ProgressStyle constant.
     */
    public void setProgressBarStyle(@ProgressBarStyle int style) {
        progressBarStyle(style);
        this.invalidate();
    }

    //############################################################################################## UPDATE PIPELINE AS A FLUENT API

    /**
     * Sets the circular drawing direction. Default orientation is ORIENTATION_CLOCKWISE.
     *
     * @param orientation non-negative orientation constant.
     * @throws IllegalArgumentException if the given orientation is not a ProgressOrientation constant or not supported by the current used chart mode.
     */
    public PercentageChartView orientation(@ProgressOrientation int orientation) {
        if (orientation != ORIENTATION_CLOCKWISE && orientation != ORIENTATION_COUNTERCLOCKWISE) {
            throw new IllegalArgumentException("Orientation must be a ProgressOrientation constant.");
        }

        try {
            ((OrientationBasedMode) renderer).setOrientation(orientation);
        } catch (ClassCastException e) {
            throw new IllegalArgumentException("Orientation is not support by the used percentage chart mode.");
        }
        return this;
    }

    /**
     * Sets the current circular drawing's start angle in degrees. Default start angle is0.
     *
     * @param startAngle A positive start angle value that is less or equal to 360.
     * @throws IllegalArgumentException if the given start angle is not positive, or, less or equal to 360.
     */
    public PercentageChartView startAngle(float startAngle) {
        if (startAngle < 0 || startAngle > 360) {
            throw new IllegalArgumentException("Start angle value must be positive and less or equal to 360.");
        }
        this.renderer.setStartAngle(startAngle);
        return this;
    }

    /**
     * Sets whether background should be drawn.
     *
     * @param enabled True if background have to be drawn, false otherwise.
     */
    public PercentageChartView drawBackgroundEnabled(boolean enabled) {
        this.renderer.setDrawBackgroundEnabled(enabled);
        return this;
    }

    /**
     * Sets the circular background color for this view.
     *
     * @param color the color of the circular background
     */
    public PercentageChartView backgroundColor(int color) {
        this.renderer.setBackgroundColor(color);
        return this;
    }

    /**
     * Sets the progress/progress bar color for this view.
     *
     * @param color the color of the progress/progress bar
     */
    public PercentageChartView progressColor(int color) {
        this.renderer.setProgressColor(color);
        return this;
    }

    /**
     * Sets progress gradient colors.
     *
     * @param type      The gradient type which is a GradientTypes constant
     * @param colors    The colors to be distributed.
     *                  There must be at least 2 colors in the array.
     * @param positions May be NULL. The relative position of
     *                  each corresponding color in the colors array, beginning
     *                  with 0 and ending with 1.0. If the values are not
     *                  monotonic, the drawing may produce unexpected results.
     *                  If positions is NULL, then the colors are automatically
     *                  spaced evenly.
     * @param angle     Defines the direction for linear gradient type.
     * @throws IllegalArgumentException If type is not a GradientTypes constant and if colors array is null
     */
    public PercentageChartView gradientColors(@GradientTypes int type, int[] colors, float[] positions, float angle) {
        if (type < GRADIENT_LINEAR || type > GRADIENT_SWEEP) {
            throw new IllegalArgumentException("Invalid value for progress gradient type.");
        } else if (colors == null) {
            throw new IllegalArgumentException("Gradient colors int array cannot be null.");
        }

        this.renderer.setGradientColors(type, colors, positions, angle);
        return this;
    }

    /**
     * Sets the duration of the progress change's animation.
     *
     * @param duration non-negative duration value.
     * @throws IllegalArgumentException if the given duration is less than 50.
     */
    public PercentageChartView animationDuration(int duration) {
        if (duration < 50) {
            throw new IllegalArgumentException("Duration must be equal or greater than 50.");
        }
        renderer.setAnimationDuration(duration);
        return this;
    }

    /**
     * Sets the interpolator of the progress change's animation.
     *
     * @param curveType Specifies the interpolator type for the animator.
     * @throws IllegalArgumentException if the given TimeInterpolator instance is null.
     */
    @SuppressWarnings("ConstantConditions")
    public PercentageChartView animationInterpolator(int curveType) {
        renderer.setAnimationInterpolator(curveType);
        return this;
    }

    /**
     * Sets the text color for this view.
     *
     * @param color the text color
     */
    public PercentageChartView textColor(int color) {
        renderer.setTextColor(color);
        return this;
    }

    /**
     * Sets the text size.
     *
     * @param size the text size
     * @throws IllegalArgumentException if the given text size is zero or a negative value.
     */
    public PercentageChartView textSize(float size) {
        if (size <= 0) {
            throw new IllegalArgumentException("Text size must be a nonzero positive value.");
        }
        renderer.setTextSize(size);
        return this;
    }

    /**
     * Sets the text font.
     *
     * @param typeface the text font as a Font instance
     * @throws IllegalArgumentException if the given Font is null.
     */
    @SuppressWarnings("ConstantConditions")
    public PercentageChartView typeface(Font.Builder typeface) {
        if (typeface == null) {
            throw new IllegalArgumentException("Text Font cannot be null");
        }
        renderer.setTypeface(typeface);
        return this;
    }

    /**
     * Sets the text style.
     *
     * @param style the text style.
     * @throws IllegalArgumentException if the given text style is not a valid TextStyle constant.
     */
    public PercentageChartView textStyle(@TextStyle int style) {
        if (style < 0 || style > 3) {
            throw new IllegalArgumentException("Text style must be a valid TextStyle constant.");
        }
        renderer.setTextStyle(style);
        return this;
    }

    /**
     * Sets the text shadow. Passing zeros will remove the shadow.
     *
     * @param shadowColor  text shadow color value.
     * @param shadowRadius text shadow radius.
     * @param shadowDistX  text shadow y-axis distance.
     * @param shadowDistY  text shadow x-axis distance.
     */
    public PercentageChartView textShadow(int shadowColor, float shadowRadius, float shadowDistX, float shadowDistY) {
        renderer.setTextShadow(shadowColor, shadowRadius, shadowDistX, shadowDistY);
        return this;
    }

    /**
     * Sets the offset of the circular background. Works only if chart mode is set to pie.
     *
     * @param offset A positive offset value.
     * @throws IllegalArgumentException if the given offset is a negative value, or, not supported by the current used chart mode.
     */
    public PercentageChartView backgroundOffset(int offset) {
        if (offset < 0) {
            throw new IllegalArgumentException("Background offset must be a positive value.");
        }

        try {
            ((OffsetEnabledMode) renderer).setBackgroundOffset(offset);
        } catch (ClassCastException e) {
            throw new IllegalArgumentException("Background offset is not support by the used percentage chart mode.");
        }
        return this;
    }

    /**
     * Sets whether background bar should be drawn.
     *
     * @param enabled True if background bar have to be drawn, false otherwise.
     * @throws IllegalArgumentException if background bar's drawing state is not supported by the current used chart mode.
     */
    public PercentageChartView drawBackgroundBarEnabled(boolean enabled) {
        try {
            ((RingModeRenderer) renderer).setDrawBackgroundBarEnabled(enabled);
        } catch (ClassCastException e) {
            throw new IllegalArgumentException("Background bar's drawing state is not support by the used percentage chart mode.");
        }
        return this;
    }

    /**
     * Sets the background bar color.
     *
     * @param color the background bar color
     * @throws IllegalArgumentException if background bar color is not supported by the current used chart mode.
     */
    public PercentageChartView backgroundBarColor(int color) {
        try {
            ((RingModeRenderer) renderer).setBackgroundBarColor(color);
        } catch (ClassCastException e) {
            throw new IllegalArgumentException("Background bar color is not support by the used percentage chart mode.");
        }
        return this;
    }

    /**
     * Sets the background bar thickness in pixels. Works only if chart mode is set to ring.
     *
     * @param thickness non-negative thickness value in pixels.
     * @throws IllegalArgumentException if the given value is negative, or, background bar thickness is not supported by the current used chart mode.
     */
    public PercentageChartView backgroundBarThickness(float thickness) {
        if (thickness < 0) {
            throw new IllegalArgumentException("Background bar thickness must be a positive value.");
        }

        try {
            ((RingModeRenderer) renderer).setBackgroundBarThickness(thickness);
        } catch (ClassCastException e) {
            throw new IllegalArgumentException("Background bar thickness is not support by the used percentage chart mode.");
        }
        return this;
    }

    /**
     * Sets the progress bar thickness in pixels. Works only if chart mode is set to ring.
     *
     * @param thickness non-negative thickness value in pixels.
     * @throws IllegalArgumentException if the given value is negative, or, progress bar thickness is not supported by the current used chart mode.
     */
    public PercentageChartView progressBarThickness(float thickness) {
        if (thickness < 0) {
            throw new IllegalArgumentException("Progress bar thickness must be a positive value.");
        }

        try {
            ((RingModeRenderer) renderer).setProgressBarThickness(thickness);
        } catch (ClassCastException e) {
            throw new IllegalArgumentException("Progress bar thickness is not support by the used percentage chart mode.");
        }
        return this;
    }

    /**
     * Sets the progress bar stroke style. Works only if chart mode is set to ring.
     *
     * @param style Progress bar stroke style as a ProgressStyle constant.
     * @throws IllegalArgumentException if the given progress bar style is not a valid ProgressBarStyle constant, or, not supported by the current used chart mode.
     */
    public PercentageChartView progressBarStyle(@ProgressBarStyle int style) {
        if (style < 0 || style > 1) {
            throw new IllegalArgumentException("Progress bar style must be a valid TextStyle constant.");
        }

        try {
            ((RingModeRenderer) renderer).setProgressBarStyle(style);
        } catch (ClassCastException e) {
            throw new IllegalArgumentException("Progress bar style is not support by the used percentage chart mode.");
        }
        return this;
    }

    /**
     * Apply all the requested changes.
     */
    public void apply() {
        this.invalidate();
    }

    //##############################################################################################   ADAPTIVE COLOR PROVIDER
    public void setAdaptiveColorProvider(AdaptiveColorProvider adaptiveColorProvider) {
        this.renderer.setAdaptiveColorProvider(adaptiveColorProvider);
    }

    //##############################################################################################   TEXT FORMATTER
    public void setTextFormatter(ProgressTextFormatter textFormatter) {
        this.renderer.setTextFormatter(textFormatter);
    }

    //##############################################################################################   LISTENER
    public void setOnProgressChangeListener(OnProgressChangeListener onProgressChangeListener) {
        this.onProgressChangeListener = onProgressChangeListener;
    }

}
