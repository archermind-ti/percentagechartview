package com.ramijemli.percentagechartview.callback;

public interface ProgressTextFormatter {
    CharSequence provideFormattedText(float progress);
}
