/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ramijemli.percentagechartview.utils;

import ohos.agp.text.Font;
import ohos.agp.utils.TextTool;
import ohos.app.Context;
import ohos.global.resource.RawFileEntry;
import ohos.global.resource.Resource;
import ohos.global.resource.ResourceManager;

import java.io.*;
import java.util.Arrays;
import java.util.List;

import static com.ramijemli.percentagechartview.utils.MyAttrsUtils.*;
import static com.ramijemli.percentagechartview.utils.MyAttrsUtils.BOLD_ITALIC;

public class FontUtils {
    private static List<String> fonts = Arrays.asList(
            "sans-serif",
            "sans-serif-medium",
            "HwChinese-medium",
            "sans-serif-condensed",
            "sans-serif-condensed-medium",
            "monospace"
            );

    /**
     * Use ApplicationContext for static methods
     *
     * @param applicationContext
     * @param typeface
     * @return
     */
    public static Font.Builder getFontBuilder(Context applicationContext, String typeface) {
        if (TextTool.isNullOrEmpty(typeface)) {
            return new Font.Builder("HwChinese-medium");
        }

        if (fonts.contains(typeface)) {
            return new Font.Builder(typeface);
        }

        File file = new File(applicationContext.getCodeCacheDir(), typeface);
        OutputStream outputStream = null;
        ResourceManager resManager = applicationContext.getResourceManager();
        RawFileEntry rawFileEntry = resManager.getRawFileEntry("resources/rawfile/fonts/" + typeface);
        Resource resource = null;
        try {
            resource = rawFileEntry.openRawFile();
            outputStream = new FileOutputStream(file);
            int index;
            byte[] bytes = new byte[1024];
            if (resource != null) {
                while ((index = resource.read(bytes)) != -1) {
                    outputStream.write(bytes, 0, index);
                    outputStream.flush();
                }
                return new Font.Builder(file);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                resource.close();
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return new Font.Builder("HwChinese-medium");
    }

    public static Font.Builder setTextStyle(Font.Builder mTypeface, int mTextStyle) {
        if (mTypeface == null) {
            mTypeface = new Font.Builder("HwChinese-medium");
        }
        switch (mTextStyle) {
            case NORMAL:
                mTypeface.setWeight(Font.REGULAR).makeItalic(false);
                break;
            case BOLD:
                mTypeface.setWeight(Font.BOLD).makeItalic(false);
                break;
            case ITALIC:
                mTypeface.setWeight(Font.REGULAR).makeItalic(true);
                break;
            case BOLD_ITALIC:
                mTypeface.setWeight(Font.BOLD).makeItalic(true);
                break;
        }
        return mTypeface;

    }
}
