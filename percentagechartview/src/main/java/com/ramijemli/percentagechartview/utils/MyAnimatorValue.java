/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ramijemli.percentagechartview.utils;

import ohos.agp.animation.AnimatorValue;


public class MyAnimatorValue<V, T extends MyAnimatorValue.Evaluator<V>> extends AnimatorValue {

    private Evaluator<V> mEvaluator;

    public static MyAnimatorValue ofObject(Evaluator evaluator, Object... args) {
        MyAnimatorValue myAnimatorValue = new MyAnimatorValue();
        myAnimatorValue.mEvaluator = evaluator;
        myAnimatorValue.mEvaluator.setInitialValues(args[0], args[1]);
        return myAnimatorValue;
    }

    public static MyAnimatorValue ofFloat(Object... args) {
        MyAnimatorValue myAnimatorValue = new MyAnimatorValue();
        myAnimatorValue.mEvaluator = new DefaultEvaluator();
        myAnimatorValue.mEvaluator.setInitialValues(args[0], args[1]);
        return myAnimatorValue;
    }

    public static MyAnimatorValue ofInt(int... args) {
        MyAnimatorValue myAnimatorValue = new MyAnimatorValue();
        myAnimatorValue.mEvaluator = new DefaultEvaluator();
        myAnimatorValue.mEvaluator.setInitialValues(args[0], args[1]);
        return myAnimatorValue;
    }


    public void setIntValues(Object... args) {
        mEvaluator.setInitialValues(args[0], args[1]);
    }

    public void setFloatValues(Object... args) {
        mEvaluator.setInitialValues(args[0], args[1]);
    }

    public TimeInterpolator getInterpolator() {
        return mInterpolator;
    }

    public static class DefaultEvaluator implements Evaluator<Float> {

        private float v;
        private Float[] floats;
        private float mDelta = 1;

        @Override
        public Float getValue() {
            return v;
        }

        @Override
        public void evaluate(float v) {
            this.v = v * mDelta + (Float) floats[0];
        }

        @Override
        public void setInitialValues(Object... args) {
            floats = new Float[] {(Float) args[0], (Float) args[1]};
            mDelta = floats[1] - floats[0];
        }
    }


    public interface Evaluator<V> {
        V getValue();
        void evaluate(float v);
        void setInitialValues(Object... args);
    }

    private TimeInterpolator mInterpolator;

    public V getAnimatedValue(){
        return mEvaluator.getValue();
    }

    public void setValueUpdateListener(ValueUpdateListener valueUpdateListener) {
        super.setValueUpdateListener(new ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                if(mInterpolator == null) {
                    mEvaluator.evaluate(v);
                    valueUpdateListener.onUpdate(animatorValue, v);
                }else {
                    mEvaluator.evaluate(mInterpolator.getInterpolation(v));
                    valueUpdateListener.onUpdate(animatorValue, mInterpolator.getInterpolation(v));
                }
            }
        });
    }

    public void setInterpolator(TimeInterpolator interpolator) {
        mInterpolator = interpolator;
    }
}
