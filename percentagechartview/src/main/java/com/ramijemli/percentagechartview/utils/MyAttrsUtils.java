/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ramijemli.percentagechartview.utils;

import java.util.HashMap;
import java.util.Map;

public class MyAttrsUtils {

    public static final String PCV_MODE = "pcv_mode";
    // CHART MODE
    public static final int MODE_RING = 0;
    public static final int MODE_PIE = 1;
    public static final int MODE_FILL = 2;
    public static final String PCV_ORIENTATION = "pcv_orientation";
    // ORIENTATION
    public static final int INVALID_ORIENTATION = -1;
    public static final int ORIENTATION_CLOCKWISE = 0;
    public static final int ORIENTATION_COUNTERCLOCKWISE = 1;
    public static final String PCV_DRAW_BACKGROUND = "pcv_drawBackground";
    public static final String PCV_START_ANGLE = "pcv_startAngle";
    public static final String PCV_BACKGROUND_COLOR = "pcv_backgroundColor";
    public static final  String PCV_PROGRESS = "pcv_progress";
    public static final  String PCV_PROGRESS_COLOR = "pcv_progressColor";
    public static final String PCV_ANIM_DURATION = "pcv_animDuration";
    public static final int LINEAR = 0;
    public static final int ACCELERATE = 1;
    public static final int DECELERATE = 2;
    public static final int ACCELERATE_DECELERATE = 3;
    public static final int ANTICIPATE = 4;
    public static final int OVERSHOOT = 5;
    public static final int ANTICIPATE_OVERSHOOT = 6;
    public static final int BOUNCE = 7;
    public static final int FAST_OUT_LINEAR_IN = 8;
    public static final int FAST_OUT_SLOW_IN = 9;
    public static final int LINEAR_OUT_SLOW_IN = 10;
    public static final String PCV_ANIM_INTERPOLATOR = "pcv_animInterpolator";
    public static final String PCV_TEXT_COLOR = "pcv_textColor";
    public static final String PCV_TEXT_SIZE = "pcv_textSize";
    public static final String PCV_TYPEFACE = "pcv_typeface";
    public static final String PCV_TEXT_STYLE = "pcv_textStyle";
    public static final int NORMAL = 0;
    public static final int BOLD = 1;
    public static final int ITALIC = 2;
    public static final int BOLD_ITALIC = 3;
    public static final String PCV_TEXT_SHADOW_COLOR = "pcv_textShadowColor";
    public static final String PCV_BACKGROUND_OFFSET = "pcv_backgroundOffset";
    public static final String PCV_GRADIENT_TYPE = "pcv_gradientType";
    // CHART MODE
    public static final int INVALID_GRADIENT = -1;
    public static final int GRADIENT_LINEAR = 0;
    public static final int GRADIENT_RADIAL = 1;
    public static final int GRADIENT_SWEEP = 2;
    public static final String PCV_GRADIENT_ANGLE = "pcv_gradientAngle";
    public static final String PCV_GRADIENT_COLORS = "pcv_gradientColors";
    public static final String PCV_GRADIENT_DISTRIBUTIONS = "pcv_gradientDistributions";

    public static final String PCV_DRAW_BACKGROUND_BAR = "pcv_drawBackgroundBar";
    public static final String PCV_BACKGROUND_BAR_THICKNESS = "pcv_backgroundBarThickness";
    public static final String PCV_BACKGROUND_BAR_COLOR = "pcv_backgroundBarColor";
    public static final String PCV_PROGRESS_BAR_THICKNESS = "pcv_progressBarThickness";
    public static final String PCV_PROGRESS_BAR_STYLE = "pcv_progressBarStyle";


    public static Map<String, Integer> pcv_mode = new HashMap<>();
    public static Map<String, Integer> pcv_orientation = new HashMap<>();
    public static Map<String, Integer> pcv_gradientType = new HashMap<>();
    public static Map<String, Integer> pcv_animInterpolator = new HashMap<>();
    public static Map<String, Integer> pcv_textStyle = new HashMap<>();
    public static Map<String, Integer> pcv_progressBarStyle = new HashMap<>();

    static {
        //pcv_mode
        pcv_mode.put("ring", MODE_RING);
        pcv_mode.put("pie", MODE_PIE);
        pcv_mode.put("fill", MODE_FILL);
        //pcv_orientation
        pcv_orientation.put("clockwise", ORIENTATION_CLOCKWISE);
        pcv_orientation.put("counter_clockwise", ORIENTATION_COUNTERCLOCKWISE);
        //pcv_gradientType
        pcv_gradientType.put("linear", GRADIENT_LINEAR);
        pcv_gradientType.put("radial", GRADIENT_RADIAL);
        pcv_gradientType.put("sweep", GRADIENT_SWEEP);
        //pcv_animInterpolator
        pcv_animInterpolator.put("linear", LINEAR);
        pcv_animInterpolator.put("accelerate", ACCELERATE);
        pcv_animInterpolator.put("decelerate", DECELERATE);
        pcv_animInterpolator.put("accelerate_decelerate", ACCELERATE_DECELERATE);
        pcv_animInterpolator.put("anticipate", ANTICIPATE);
        pcv_animInterpolator.put("overshoot", OVERSHOOT);
        pcv_animInterpolator.put("anticipate_overshoot", ANTICIPATE_OVERSHOOT);
        pcv_animInterpolator.put("bounce", BOUNCE);
        pcv_animInterpolator.put("fast_out_linear_in", FAST_OUT_LINEAR_IN);
        pcv_animInterpolator.put("fast_out_slow_in", FAST_OUT_SLOW_IN);
        pcv_animInterpolator.put("linear_out_slow_in", LINEAR_OUT_SLOW_IN);
        //pcv_textStyle
        pcv_textStyle.put("normal", NORMAL);
        pcv_textStyle.put("bold", BOLD);
        pcv_textStyle.put("italic", ITALIC);
        pcv_textStyle.put("bold_italic", BOLD_ITALIC);
        //pcv_progressBarStyle
        pcv_progressBarStyle.put("round", 0);
        pcv_progressBarStyle.put("square", 1);
    }
}
