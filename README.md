# Percentage Chart View
### 项目简介

**一个多样式百分比进度条实现库，包括环，饼，填充式**

### 功能演示
#### RING MODE

![](art/showcase_1.gif)
![](art/showcase_2.gif)
![](art/showcase_3.gif)
![](art/showcase_4.gif)

#### PIE MODE

![](art/showcase_5.gif)
![](art/showcase_6.gif)
![](art/showcase_7.gif)
![](art/showcase_8.gif)

#### FILL MODE

![](art/showcase_9.gif)
![](art/showcase_10.gif)
![](art/showcase_11.gif)
![](art/showcase_12.gif)

### 集成说明
#### 方式一

1. 下载PercentageChartView的har包PercentageChartView.har（位于output文件夹下）。
2. 启动DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的应用。

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
      implementation project(':PercentageChartView')
	……
}
```

4.在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

#### 方式二

1. 在module的build.gradle中添加对`PercentageChartView`的依赖

    ```groovy
   dependencies {
       implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    
       ……
       implementation 'com.gitee.archermind-ti:PercentageChartView:1.0.0'
   }
   ```

2. 在project的build.gradle中添加`mavenCentral()`的引用

   ``` groovy
   allprojects {
       repositories {
           ……
           mavenCentral()
       }
   }
   ```
   
### 使用说明：

```  
    <com.ramijemli.percentagechartview.PercentageChartView
        ohos:id="@+id/view_id"
        ohos:layout_width="match_parent"
        ohos:layout_height="match_parent"
        app:pcv_mode="pie"
        app:pcv_orientation="counter_clockwise"
        app:pcv_animDuration="800"
        app:pcv_animInterpolator="anticipate_overshoot"
        app:pcv_progress="10"
        app:pcv_startAngle="90"/>
  ```  
### 功能说明
#### Attributes 
 
| Name | Format | Default | Supported modes | Description |
| --- | :---: | :---: | :---: | --- | 
| `pcv_mode` | `enum` | `pie` | - | Sets percentage chart appearance to **`"ring"`**, **`"pie"`**, or **`"fill"`**. |  
| `pcv_orientation` | `enum` | `clockwise` | Pie, Ring | Sets progress's drawing direction to **`"clockwise"`** or **`"counter_clockwise"`**. |  
| `pcv_startAngle` | `integer` | `0` | All | Sets progress's drawing start angle to **\[0..360]**. |  
| `pcv_animDuration` | `integer` | `400` | All | Sets progress update's animation duration. |  
| `pcv_animInterpolator` | `enum` | `linear` | All | Sets progress update's animation interpolator to **`"linear"`**, **`"accelerate"`**, **`"decelerate"`**, **`"accelerate_decelerate"`**, **`"anticipate"`**, **`"overshoot"`**, **`"anticipate_overshoot"`**, **`"bounce"`**. | 
| `pcv_drawBackground` | `boolean` | `true` for pie mode <br/>`false` for ring mode | All | Sets whether to draw background or not. |  
| `pcv_backgroundColor` | `color` | `#000000` | All | Sets background color. |  
| `pcv_progress` | `integer` | `0` | All | Sets current progress. |  
| `pcv_progressColor` | `color` | Accent color | All | Sets progress color. |  
| `pcv_textColor` | `color` | `#ffffff` | All | Sets text color. |  
| `pcv_textSize` | `dimension` | `#12sp` | All | Sets text size in SP. |  
| `pcv_typeface` | `string` | System font | All | Sets progress text's typeface file path in assets folder. |  
| `pcv_textStyle` | `flag` | `normal` | All | Sets progress text's style to **`"normal"`**, **`"bold"`**, **`"italic"`**, **`"bold\|italic"`**. |  
| `pcv_textShadowColor` |  `color` | `#00ffffff` | All | Sets text shadow/glow color. |  
| `pcv_textShadowRadius` | `string` | `0` | All | Sets text shadow/glow radius. |  
| `pcv_textShadowDistX` | `float` | `0` | All | Sets text shadow/glow's x-axis distance. |  
| `pcv_textShadowDistY` | `float` | `0` | All | Sets text shadow/glow's y-axis distance. |  
| `pcv_backgroundOffset` | `dimension` | `0dp` | Pie, Fill | Sets a margin only for background. |  
| `pcv_drawBackgroundBar` | `boolean` | `true` | Ring | Sets whether to draw background bar or not. |  
| `pcv_backgroundBarThickness` | `dimension` | `16dp` | Ring | Sets background bar's thickness in DP. |  
| `pcv_backgroundBarColor` | `color` | `#000000` | Ring | Sets background color. |  
| `pcv_progressBarThickness` | `dimension` | `16dp` | Ring | Sets progress bar's thickness in DP. |  
| `pcv_progressBarStyle` | `enum` | `round` | Ring | Sets progress bar's style to **`"round"`** or **`"square"`**. |  
| `pcv_gradientType` | `enum` | - | All | Sets the gradient colors' type for progress to **`"linear"`**, **`"radial"`**, or **`"sweep"`**. (sweep is not supported for fill mode) |  
| `pcv_gradientColors` | `string` | - | All | Sets the gradient colors for progress in a comma separated hex color values format; **`"#F44336 , #2196F3 , #00BCD4"`**. |  
| `pcv_gradientDistributions` | `string` | - | All | Sets the gradient colors' distribution in a comma separated float values format; **`"0.2 , 0.5 , 0.8"`**.<br/> Values must be monotonic and belong to **\[0..1]**. If ignored colors will be distributed evenly.  |  
| `pcv_gradientAngle` | `integer` | `pcv_startAngle` | All | Sets linear gradient colors' drawing angle to **\[0..360]**. |  

#### 开始角度

`pcv_startAngle` 和 `pcv_gradientAngle` 属性接受下一个插图的值。

<img src="art/angle.png" width="30%"/>

#### 流程使用

除了pcv_mode属性(目前)，所有XML属性都有对应的Java属性。

可以通过进行更改和调用apply()来设置更新。这将确保重新绘制视图
```
    mChart.textColor(Color.BLACK)  
          .textSize(sizeSp)  
          .typeface(typeface)  
          .textShadow(Color.WHITE, 2f, 2f, 2f)  
          .progressColor(Color.RED)  
          .backgroundColor(Color.BLACK)  
      .apply();
```
对于单个更新，可以调用所需的setter方法。例如`setTextSize (sizeSp)`。

### 进度条基本的自适应颜色

要使用每个进度的颜色特性，你必须使用**`setAdaptiveColorProvider()`**方法传递一个**`AdaptiveColorProvider`**类。
自适应颜色可以应用于进度、背景、文本和背景栏。
值得一提的是，梯度颜色的优先级高于所提供的颜色，您可以忽略重新定义不需要的方法，因为它们默认有一个实现。
  
``` 
AdaptiveColorProvider colorProvider = new AdaptiveColorProvider() {  
    @Override  
    public int provideProgressColor(float progress) {  
        if (progress <= 25)  
            return colorOne;  
        else if (progress <= 50)  
            return colorTwo;  
        else if (progress <= 75)  
            return colorThree;  
        else return colorFour;  
    }
  
    @Override  
    public int provideBackgroundColor(float progress) {  
		//This will provide a bg color that is 80% darker than progress color.
        return ColorUtils.blendARGB(provideProgressColor(progress), Color.BLACK, .8f);  
    }  
  
    @Override  
    public int provideTextColor(float progress) {  
        return provideProgressColor(progress);  
    }
  
    @Override  
    public int provideBackgroundBarColor(float progress) {  
        return ColorUtils.blendARGB(provideProgressColor(progress), Color.BLACK, .5f);  
    }  
};

mPieChart.setAdaptiveColorProvider(colorProvider);
```

### 进度改变监听

可以通过设置一个**`OnProgressChangeListener`**来获得进度更新。
``` 
chart.setOnProgressChangeListener(new PercentageChartView.OnProgressChangeListener() {
    @Override
    public void onProgressChanged(float progress) {
        Log.d(TAG, String.valueOf(progress));
    }
});
``` 

### 文本格式化

对于文本，可以使用其他单位代替百分比。你必须使用**`setTextFormatter()`**方法传递一个**`ProgressTextFormatter`**类。< br / >
``` 
mRingChart.setTextFormatter(new ProgressTextFormatter() {  
    @Override  
    public String provideFormattedText(float progress) {  
        int days = (int) (progress * maxDays / 100);  
        return days + " days";  
    }  
});
```

### 版本迭代

- v1.0.0

License
-------
```
   Copyright 2019 Rami Jemli
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
       http://www.apache.org/licenses/LICENSE-2.0
   
   Unless required by applicable law or agreed to in writing, 
   software distributed under the License is distributed on an "AS IS" BASIS, 
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
   See the License for the specific language governing permissions 
   and limitations under the License.
```