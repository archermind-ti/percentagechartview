package com.ramijemli.sample.adapter;

import com.ramijemli.sample.ResourceTable;
import com.ramijemli.sample.util.ColorUtils;
import com.ramijemli.sample.viewmodel.Showcase;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;

import java.util.Arrays;
import java.util.HashMap;

public class HomeAdapter extends BaseItemProvider {

    private final int VIEW_TYPE_SHOWCASE = ResourceTable.Layout_item_home;
    private final int VIEW_TYPE_DEV = ResourceTable.Layout_item_dev;

    private Context mContext;
    private HashMap<Integer, Showcase> mData;
    private OnHomeClickedListener mListener;

    public HomeAdapter(Context context) {
        this.mContext = context;
        mData = new HashMap<>();
        int[] colors = Arrays.stream(mContext.getStringArray(ResourceTable.Strarray_home_colors))
                .mapToInt(colorString -> Integer.parseInt(colorString.replace("#",""), 16)).toArray();

        String[] titles = mContext.getStringArray(ResourceTable.Strarray_home_titles);
        String[] subtitles = mContext.getStringArray(ResourceTable.Strarray_home_subtitles);

        for (int i = 0; i < colors.length; i++) {
            mData.put(i, new Showcase(colors[i], titles[i], subtitles[i]));
        }

        mData.put(colors.length, new Showcase(0x37474F, "", ""));
    }

    public HomeAdapter.BaseViewHolder createViewHolder(Component component, int viewType, int position) {
        switch (viewType) {
            default:
            case VIEW_TYPE_SHOWCASE:
                return new ShowcaseViewHolder(component, position);
            case VIEW_TYPE_DEV:
                return new DevViewHolder(component, position);
        }
    }

    private int getItemViewType(int position) {
        if (position == mData.size() - 1) return VIEW_TYPE_DEV;
        return VIEW_TYPE_SHOWCASE;
    }

    @Override
    public int getCount() {
        return mData == null ? 0 : mData.size();
    }

    @Override
    public Object getItem(int position) {
        if (mData != null && position >= 0 && position < mData.size()) {
            return mData.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        Component cpt;
        BaseViewHolder viewHolder;
        if (component == null) {
            cpt = LayoutScatter.getInstance(mContext).parse(getItemViewType(position), null, false);
            viewHolder = createViewHolder(cpt, getItemViewType(position), position);
            cpt.setTag(viewHolder);
        } else {
            cpt = component;
            viewHolder = (ShowcaseViewHolder) cpt.getTag();
        }

        Showcase data = mData.get(position);
        viewHolder.bind(data);

        return cpt;
    }

    //##############################################################################################   VIEW HOLDERS
    class BaseViewHolder implements Component.ClickedListener {
        Component card;
        int position;

        BaseViewHolder(Component itemView, int position) {
            this.position = position;
            card = itemView.findComponentById(ResourceTable.Id_card_bg);
        }

        void bind(Showcase data){
            ShapeElement shapeElement = new ShapeElement();
            shapeElement.setRgbColor(new RgbColor(ColorUtils.red(data.getColor()),
                    ColorUtils.green(data.getColor()),
                    ColorUtils.blue(data.getColor()))
            );
            shapeElement.setCornerRadius(20);
            card.setBackground(shapeElement);
            card.setClickedListener(this);
        }

        @Override
        public void onClick(Component v) {
            if (mListener != null) {
                mListener.onItemClicked(position);
            }
        }
    }

    class ShowcaseViewHolder extends BaseViewHolder {
        Text title;
        Text subtitle;

        ShowcaseViewHolder(Component itemView, int position) {
            super(itemView, position);
            title = (Text) itemView.findComponentById(ResourceTable.Id_title);
            subtitle = (Text) itemView.findComponentById(ResourceTable.Id_subtitle);
        }

        void bind(Showcase data) {
            super.bind(data);
            title.setText(data.getTitle());
            subtitle.setText(data.getSubtitle());
        }
    }

    class DevViewHolder extends BaseViewHolder {

        DevViewHolder(Component itemView, int position) {
            super(itemView, position);
        }

        void bind(Showcase data) {
            super.bind(data);
        }
    }

    //##############################################################################################   CLICK LISTENER
    public void setOnHomeClickedListener(OnHomeClickedListener mListener) {
        this.mListener = mListener;
    }

    public interface OnHomeClickedListener {
        void onItemClicked(int position);
    }
}

