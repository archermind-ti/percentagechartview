package com.ramijemli.sample.adapter;

import com.ramijemli.sample.util.FractionInterface;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

import java.util.List;

public class ProjectPageSliderProvider extends PageSliderProvider {
    @Override
    public String getPageTitle(int position) {
        final FractionWrapper fractionWrapper = list.get(position);
        return fractionWrapper.behavior;
    }

    private List<FractionWrapper> list;
    public ProjectPageSliderProvider(List<FractionWrapper> list) {
        this.list = list;
    }
    @Override
    public int getCount() {
        return list.size();
    }
    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int i) {
        final FractionWrapper fractionWrapper = list.get(i);
        FractionInterface fraction = (FractionInterface) fractionWrapper.getFraction();
        fraction.onStart(fractionWrapper.getIntent());
        Component component = fraction.onComponentAttached(LayoutScatter.getInstance(componentContainer.getContext()), componentContainer, fractionWrapper.getIntent() );
        componentContainer.addComponent(component);
        return component;
    }
    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
        componentContainer.removeComponent((Component) o);
        final FractionWrapper fractionWrapper = list.get(i);
        FractionInterface fraction = (FractionInterface) fractionWrapper.getFraction();
        fraction.onComponentDetach();
        fraction.onStop();
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object o) {
        return true;
    }

    public static class FractionWrapper {
        private String behavior;
        private Class<?> subFragmentClass;
        private Fraction fraction;
        private Intent intent;

        public FractionWrapper(String behavior, Class<?> SubFragmentClass, Intent intent) {
            this.behavior = behavior;
            this.subFragmentClass = SubFragmentClass;
            this.intent = intent;

            try {
                fraction = (Fraction) subFragmentClass.newInstance();
            } catch (IllegalAccessException | InstantiationException e) {
                e.printStackTrace();
            }
        }

        public FractionWrapper(String behavior, Class<?> SubFragmentClass) {
            this.behavior = behavior;
            this.subFragmentClass = SubFragmentClass;

            try {
                fraction = (Fraction) subFragmentClass.newInstance();
            } catch (IllegalAccessException | InstantiationException e) {
                e.printStackTrace();
            }
        }

        public Fraction getFraction() {
            return fraction;
        }

        public Intent getIntent() {
            return intent;
        }
    }
}
