package com.ramijemli.sample.util;


import ohos.agp.components.Attr;
import ohos.agp.components.AttrHelper;
import ohos.app.Context;

public class ScreenUtil {

    public static int getRealScreenWidthInPx(Context context) {
        return 0;
    }

    public static int convertDIPToPixels(Context context, int dip) {
        return AttrHelper.vp2px(dip, AttrHelper.getDensity(context));
    }

    public static float convertPixelsToDIP(Context context, int pixels) {
        return pixels / (AttrHelper.getDensity(context)/ 160f);
    }

}