package com.ramijemli.sample.util;

import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

public interface FractionInterface {
    void onStart(Intent intent);

    void onStop();

    void onComponentDetach();

    Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent);
}
