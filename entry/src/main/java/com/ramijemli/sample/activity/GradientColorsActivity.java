package com.ramijemli.sample.activity;

import butterknife.BindComponent;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.ramijemli.percentagechartview.PercentageChartView;
import com.ramijemli.sample.ResourceTable;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.transition.Transition;
import ohos.agp.utils.Color;

import java.util.Random;

import static com.ramijemli.percentagechartview.utils.MyAttrsUtils.*;


public class GradientColorsActivity extends FractionAbility {


    @BindComponent(ResourceTable.Id_main_layout)
    ComponentContainer mConstraintLayout;

    @BindComponent(ResourceTable.Id_pie_chart)
    PercentageChartView mPieChart;
    @BindComponent(ResourceTable.Id_ring_chart)
    PercentageChartView mRingChart;
    @BindComponent(ResourceTable.Id_fill_chart)
    PercentageChartView mFillChart;

    //GRADIENT TYPE
    @BindComponent(ResourceTable.Id_gradient_value)
    RadioContainer mGradientValue;

    //GRADIENT ANGLE
    @BindComponent(ResourceTable.Id_gradient_angle_value)
    Slider mGradientAngleValue;
    @BindComponent(ResourceTable.Id_gradient_angle_value_label)
    Text mGradientAngleLabel;

    //GRADIENT COLORS
    @BindComponent(ResourceTable.Id_color_one)
    Button mColorOne;
    @BindComponent(ResourceTable.Id_color_two)
    Button mColorTwo;
    @BindComponent(ResourceTable.Id_color_three)
    Button mColorThree;
    @BindComponent(ResourceTable.Id_color_four)
    Button mColorFour;


    private Unbinder unbinder;
    //    private ConstraintSet mConstraintSet;
    private Transition fade;
    private int colorOne;
    private int colorTwo;
    private int colorThree;
    private int colorFour;
    private int displayedMode;
    private int gradientType;
    private int gradientAngle;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_activity_gradient_colors);
        unbinder = ButterKnife.bind(this,
                findComponentById(ResourceTable.Id_main_layout)
        );
        setupLayoutAnimation();
        setupGradientColors();
    }

    @Override
    public void onStop() {
        mGradientValue.setMarkChangedListener(null);
        mGradientAngleValue.setValueChangedListener(null);
        unbinder.unbind();
        unbinder = null;
        super.onStop();
    }

    private void setupLayoutAnimation() {
//        Explode transition = new Explode();
//        transition.setDuration(600);
//        transition.setInterpolator(new OvershootInterpolator(1f));
//        getWindow().setEnterTransition(transition);
//
        displayedMode = MODE_PIE;
//
//        mConstraintSet = new ConstraintSet();
//        mConstraintSet.clone(mConstraintLayout);
//
//        fade = new Fade();
//        fade.setDuration(400);
    }

    private void setupGradientColors() {
        colorOne = Color.getIntColor("#F44336");
        colorTwo = Color.getIntColor("#FFEA00");
        colorThree = Color.getIntColor("#03A9F4");
        colorFour = Color.getIntColor("#00E676");

        gradientType = GRADIENT_LINEAR;
        gradientAngle = 90;

        setGradientColors(gradientType,
                new int[]{colorOne, colorTwo, colorThree, colorFour}, null, gradientAngle);

        //GRADIENT TYPE
        mGradientValue.setMarkChangedListener((group, var2) -> {
            switch (group.getComponentAt(group.getMarkedButtonId()).getId()) {
                default:
                case ResourceTable.Id_linear:
                    gradientType = GRADIENT_LINEAR;
                    break;

                case ResourceTable.Id_radial:
                    gradientType = GRADIENT_RADIAL;
                    break;

                case ResourceTable.Id_sweep:
                    gradientType = GRADIENT_SWEEP;
                    break;
            }

            setGradientColors(gradientType, new int[]{colorOne, colorTwo, colorThree, colorFour}, null, 90);
        });

        //GRADIENT ANGLE
        mGradientAngleValue.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                gradientAngle = i;
                setGradientColors(gradientType, new int[]{colorOne, colorTwo, colorThree, colorFour}, null, gradientAngle);
                mGradientAngleLabel.setText(String.valueOf(i));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }

        });
    }

    private void setGradientColors(int type, int[] colors, float[] positions, float angle) {
        switch (displayedMode) {
            case MODE_PIE:
                mPieChart.setGradientColors(type, colors, positions, angle);
                break;
            case MODE_FILL:
                mFillChart.setGradientColors(type, colors, positions, angle);
                break;
            case MODE_RING:
                mRingChart.setGradientColors(type, colors, positions, angle);
                break;
        }
    }

    private void changeMode(int pendingMode) {
        switch (pendingMode) {
            case MODE_PIE:
                findComponentById(ResourceTable.Id_pie_chart).setVisibility(Component.VISIBLE);
                findComponentById(ResourceTable.Id_fill_chart).setVisibility(Component.HIDE);
                findComponentById(ResourceTable.Id_ring_chart).setVisibility(Component.HIDE);
                break;
            case MODE_FILL:
                findComponentById(ResourceTable.Id_pie_chart).setVisibility(Component.HIDE);
                findComponentById(ResourceTable.Id_fill_chart).setVisibility(Component.VISIBLE);
                findComponentById(ResourceTable.Id_ring_chart).setVisibility(Component.HIDE);
                break;
            case MODE_RING:
                findComponentById(ResourceTable.Id_pie_chart).setVisibility(Component.HIDE);
                findComponentById(ResourceTable.Id_fill_chart).setVisibility(Component.HIDE);
                findComponentById(ResourceTable.Id_ring_chart).setVisibility(Component.VISIBLE);
                break;
        }

        displayedMode = pendingMode;
//        applyTransition();
    }

//    private void applyTransition() {
//        TransitionManager.beginDelayedTransition(mConstraintLayout, fade);
//        mConstraintSet.applyTo(mConstraintLayout);
//    }

    //##############################################################################################   ACTIONS
    @OnClick(ResourceTable.Id_pie_chart)
    public void pieChartClickAction() {
        mPieChart.setProgress(new Random().nextInt(100), true);
    }

    @OnClick(ResourceTable.Id_ring_chart)
    public void ringChartClickAction() {
        mRingChart.setProgress(new Random().nextInt(100), true);
    }

    @OnClick(ResourceTable.Id_fill_chart)
    public void fillChartClickAction() {
        mFillChart.setProgress(new Random().nextInt(100), true);
    }

    @OnClick(ResourceTable.Id_pie_mode)
    public void pieModeAction() {
        if (displayedMode == MODE_PIE) return;
        changeMode(MODE_PIE);
    }

    @OnClick(ResourceTable.Id_ring_mode)
    public void ringModeAction() {
        if (displayedMode == MODE_RING) return;
        changeMode(MODE_RING);
    }

    @OnClick(ResourceTable.Id_fill_mode)
    public void fillModeAction() {
        if (displayedMode == MODE_FILL) return;
        changeMode(MODE_FILL);
    }

    @OnClick(ResourceTable.Id_color_one)
    public void colorOneAction() {
        ColorPickerDialogBuilder
                .with(this, 2)
                .setTitle("Choose first color")
                .setStats(1)
                .setSelectColorInterface(new OnColorSelectedListener() {

                    @Override
                    public void onColorSelected(int i) {

                    }

                    @Override
                    public void onColorMakesure(int i) {
                        colorOne = i;
                        ShapeElement shapeElement = new ShapeElement();
                        shapeElement.setRgbColor(RgbColor.fromArgbInt(colorOne));
                        mColorOne.setBackground(shapeElement);
                        setGradientColors(gradientType, new int[]{colorOne, colorTwo, colorThree, colorFour}, null, gradientAngle);
                    }
                });
    }

    @OnClick(ResourceTable.Id_color_two)
    public void colorTwoAction() {
        ColorPickerDialogBuilder
                .with(this, 2)
                .setTitle("Choose second color")
                .setStats(1)
                .setSelectColorInterface(new OnColorSelectedListener() {

                    @Override
                    public void onColorSelected(int i) {

                    }

                    @Override
                    public void onColorMakesure(int i) {
                        colorTwo = i;
                        ShapeElement shapeElement = new ShapeElement();
                        shapeElement.setRgbColor(RgbColor.fromArgbInt(colorTwo));
                        mColorTwo.setBackground(shapeElement);
                        setGradientColors(gradientType, new int[]{colorOne, colorTwo, colorThree, colorFour}, null, gradientAngle);
                    }
                });
    }

    @OnClick(ResourceTable.Id_color_three)
    public void colorThreeAction() {
        ColorPickerDialogBuilder
                .with(this, 2)
                .setTitle("Choose three color")
                .setStats(1)
                .setSelectColorInterface(new OnColorSelectedListener() {

                    @Override
                    public void onColorSelected(int i) {

                    }

                    @Override
                    public void onColorMakesure(int i) {
                        colorThree = i;
                        ShapeElement shapeElement = new ShapeElement();
                        shapeElement.setRgbColor(RgbColor.fromArgbInt(colorThree));
                        mColorThree.setBackground(shapeElement);
                        setGradientColors(gradientType, new int[]{colorOne, colorTwo, colorThree, colorFour}, null, gradientAngle);
                    }
                });
    }

    @OnClick(ResourceTable.Id_color_four)
    public void colorFourAction() {
        ColorPickerDialogBuilder
                .with(this, 2)
                .setTitle("Choose four color")
                .setStats(1)
                .setSelectColorInterface(new OnColorSelectedListener() {

                    @Override
                    public void onColorSelected(int i) {

                    }

                    @Override
                    public void onColorMakesure(int i) {
                        colorFour = i;
                        ShapeElement shapeElement = new ShapeElement();
                        shapeElement.setRgbColor(RgbColor.fromArgbInt(colorFour));
                        mColorFour.setBackground(shapeElement);
                        setGradientColors(gradientType, new int[]{colorOne, colorTwo, colorThree, colorFour}, null, gradientAngle);
                    }
                });
    }

}
