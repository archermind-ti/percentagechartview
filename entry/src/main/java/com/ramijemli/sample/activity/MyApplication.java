package com.ramijemli.sample.activity;

import ohos.aafwk.ability.AbilityPackage;
import ohos.app.Context;

public class MyApplication extends AbilityPackage {
    public Context getContxt(){
        return this.getAbilityPackageContext();
    }
}
