package com.ramijemli.sample.activity;

import butterknife.BindComponent;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.ramijemli.percentagechartview.PercentageChartView;
import com.ramijemli.percentagechartview.callback.AdaptiveColorProvider;
import com.ramijemli.sample.ResourceTable;
import com.ramijemli.sample.util.ColorUtils;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.transition.Transition;
import ohos.agp.utils.Color;

import java.util.Random;

import static com.ramijemli.percentagechartview.utils.MyAttrsUtils.*;


public class AdaptiveColorsActivity extends FractionAbility {

    @BindComponent(ResourceTable.Id_main_layout)
    ComponentContainer mConstraintLayout;

    @BindComponent(ResourceTable.Id_pie_chart)
    PercentageChartView mPieChart;
    @BindComponent(ResourceTable.Id_ring_chart)
    PercentageChartView mRingChart;
    @BindComponent(ResourceTable.Id_fill_chart)
    PercentageChartView mFillChart;

    //PROVIDED COLORS
    @BindComponent(ResourceTable.Id_color_one)
    Button mColorOne;
    @BindComponent(ResourceTable.Id_color_two)
    Button mColorTwo;
    @BindComponent(ResourceTable.Id_color_three)
    Button mColorThree;
    @BindComponent(ResourceTable.Id_color_four)
    Button mColorFour;

    private Unbinder unbinder;
    //    private ConstraintSet mConstraintSet;
    private Transition fade;
    private AdaptiveColorProvider colorProvider;
    private int colorOne;
    private int colorTwo;
    private int colorThree;
    private int colorFour;
    private int displayedMode;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_activity_adaptive_colors);
        unbinder = ButterKnife.bind(this,
                findComponentById(ResourceTable.Id_main_layout)
        );
        setupLayoutAnimation();
        setupColorProvider();
    }

    @Override
    public void onStop() {
        colorProvider = null;
        unbinder.unbind();
        unbinder = null;
        super.onStop();
    }

    private void setupLayoutAnimation() {
//        Explode transition = new Explode();
//        transition.setDuration(600);
//        transition.setInterpolator(new OvershootInterpolator(1f));
//        getWindow().setEnterTransition(transition);
//
        displayedMode = MODE_PIE;
//
//        mConstraintSet = new ConstraintSet();
//        mConstraintSet.clone(mConstraintLayout);
//
//        fade = new Fade();
//        fade.setDuration(400);
    }

    private void setupColorProvider() {
        colorOne = Color.getIntColor("#F44336");
        colorTwo = Color.getIntColor("#FFEA00");
        colorThree = Color.getIntColor("#03A9F4");
        colorFour = Color.getIntColor("#00E676");

        //COLOR PROVIDER
        colorProvider = new AdaptiveColorProvider() {
            @Override
            public int provideProgressColor(float progress) {
                if (progress <= 25)
                    return colorOne;
                else if (progress <= 50)
                    return colorTwo;
                else if (progress <= 75)
                    return colorThree;
                else
                    return colorFour;
            }

            @Override
            public int provideBackgroundColor(float progress) {
                return ColorUtils.blendARGB(provideProgressColor(progress),
                        Color.BLACK.getValue(),
                        .8f);
            }

            @Override
            public int provideTextColor(float progress) {
                if (displayedMode == MODE_RING) {
                    return provideProgressColor(progress);
                }

                return ColorUtils.blendARGB(provideProgressColor(progress),
                        Color.WHITE.getValue(),
                        .8f);
            }

            @Override
            public int provideBackgroundBarColor(float progress) {
                return ColorUtils.blendARGB(provideProgressColor(progress),
                        Color.BLACK.getValue(),
                        .5f);
            }
        };

        mPieChart.setAdaptiveColorProvider(colorProvider);
        mRingChart.setAdaptiveColorProvider(colorProvider);
        mFillChart.setAdaptiveColorProvider(colorProvider);
    }

    private void changeMode(int pendingMode) {
        switch (pendingMode) {
            case MODE_PIE:
                findComponentById(ResourceTable.Id_pie_chart).setVisibility(Component.VISIBLE);
                findComponentById(ResourceTable.Id_fill_chart).setVisibility(Component.HIDE);
                findComponentById(ResourceTable.Id_ring_chart).setVisibility(Component.HIDE);
                break;
            case MODE_FILL:
                findComponentById(ResourceTable.Id_pie_chart).setVisibility(Component.HIDE);
                findComponentById(ResourceTable.Id_fill_chart).setVisibility(Component.VISIBLE);
                findComponentById(ResourceTable.Id_ring_chart).setVisibility(Component.HIDE);
                break;
            case MODE_RING:
                findComponentById(ResourceTable.Id_pie_chart).setVisibility(Component.HIDE);
                findComponentById(ResourceTable.Id_fill_chart).setVisibility(Component.HIDE);
                findComponentById(ResourceTable.Id_ring_chart).setVisibility(Component.VISIBLE);
                break;
        }

        displayedMode = pendingMode;
//        applyTransition();
    }

//    private void applyTransition() {
//        TransitionManager.beginDelayedTransition(mConstraintLayout, fade);
//        mConstraintSet.applyTo(mConstraintLayout);
//    }

    //##############################################################################################   ACTIONS
    @OnClick(ResourceTable.Id_pie_chart)
    public void pieChartClickAction() {
        mPieChart.setProgress(new Random().nextInt(100), true);
    }

    @OnClick(ResourceTable.Id_ring_chart)
    public void ringChartClickAction() {
        mRingChart.setProgress(new Random().nextInt(100), true);
    }

    @OnClick(ResourceTable.Id_fill_chart)
    public void fillChartClickAction() {
        mFillChart.setProgress(new Random().nextInt(100), true);
    }

    @OnClick(ResourceTable.Id_pie_mode)
    public void pieModeAction() {
        if (displayedMode == MODE_PIE) return;
        changeMode(MODE_PIE);
    }

    @OnClick(ResourceTable.Id_ring_mode)
    public void ringModeAction() {
        if (displayedMode == MODE_RING) return;
        changeMode(MODE_RING);
    }

    @OnClick(ResourceTable.Id_fill_mode)
    public void fillModeAction() {
        if (displayedMode == MODE_FILL) return;
        changeMode(MODE_FILL);
    }


    @OnClick(ResourceTable.Id_color_one)
    public void colorOneAction() {
        ColorPickerDialogBuilder
                .with(this, 2)
                .setTitle("Choose first color")
                .setStats(1)
                .setSelectColorInterface(new OnColorSelectedListener() {

                    @Override
                    public void onColorSelected(int i) {

                    }

                    @Override
                    public void onColorMakesure(int i) {
                        colorOne = i;
                        ShapeElement shapeElement = new ShapeElement();
                        shapeElement.setRgbColor(RgbColor.fromArgbInt(colorOne));
                        mColorOne.setBackground(shapeElement);
                    }
                });
    }

    @OnClick(ResourceTable.Id_color_two)
    public void colorTwoAction() {
        ColorPickerDialogBuilder
                .with(this, 2)
                .setTitle("Choose second color")
                .setStats(1)
                .setSelectColorInterface(new OnColorSelectedListener() {

                    @Override
                    public void onColorSelected(int i) {

                    }

                    @Override
                    public void onColorMakesure(int i) {
                        colorTwo = i;
                        ShapeElement shapeElement = new ShapeElement();
                        shapeElement.setRgbColor(RgbColor.fromArgbInt(colorTwo));
                        mColorTwo.setBackground(shapeElement);
                    }
                });
    }

    @OnClick(ResourceTable.Id_color_three)
    public void colorThreeAction() {
        ColorPickerDialogBuilder
                .with(this, 2)
                .setTitle("Choose three color")
                .setStats(1)
                .setSelectColorInterface(new OnColorSelectedListener() {

                    @Override
                    public void onColorSelected(int i) {

                    }

                    @Override
                    public void onColorMakesure(int i) {
                        colorThree = i;
                        ShapeElement shapeElement = new ShapeElement();
                        shapeElement.setRgbColor(RgbColor.fromArgbInt(colorThree));
                        mColorThree.setBackground(shapeElement);
                    }
                });
    }

    @OnClick(ResourceTable.Id_color_four)
    public void colorFourAction() {
        ColorPickerDialogBuilder
                .with(this, 2)
                .setTitle("Choose four color")
                .setStats(1)
                .setSelectColorInterface(new OnColorSelectedListener() {

                    @Override
                    public void onColorSelected(int i) {

                    }

                    @Override
                    public void onColorMakesure(int i) {
                        colorFour = i;
                        ShapeElement shapeElement = new ShapeElement();
                        shapeElement.setRgbColor(RgbColor.fromArgbInt(colorFour));
                        mColorFour.setBackground(shapeElement);
                    }
                });
    }

}
