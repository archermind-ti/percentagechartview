package com.ramijemli.sample.activity;

import butterknife.BindComponent;
import com.ramijemli.percentagechartview.PercentageChartView;
import com.ramijemli.percentagechartview.callback.AdaptiveColorProvider;

import java.util.Random;

import com.ramijemli.sample.ResourceTable;
import com.ramijemli.sample.util.ColorUtils;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
//import org.w3c.dom.Text;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class TextFormatterActivity extends FractionAbility {

    @BindComponent(ResourceTable.Id_pie_chart)
    PercentageChartView mPieChart;
    @BindComponent(ResourceTable.Id_ring_chart)
    PercentageChartView mRingChart;
    @BindComponent(ResourceTable.Id_fill_chart)
    PercentageChartView mFillChart;
    @BindComponent(ResourceTable.Id_text)
    Text mText;

    private Unbinder unbinder;
    private AdaptiveColorProvider colorProvider;
    private int maxVoters = 10000;
    private int maxCalories = 3500;
    private int maxDays = 30;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_activity_text_formatter);
        unbinder = ButterKnife.bind(this,
                findComponentById(ResourceTable.Id_main_layout)
        );
        setupLayout();
        setupColorProvider();
    }

    @Override
    public void onStop() {
        colorProvider = null;
        unbinder.unbind();
        unbinder = null;
        super.onStop();
    }

    private void setupLayout() {
//        Explode transition = new Explode();
//        transition.setDuration(600);
//        transition.setInterpolator(new OvershootInterpolator(1f));
//        getWindow().setEnterTransition(transition);

        mPieChart.setTextFormatter(progress -> {
            int voters = (int) (progress * maxVoters / 100);
            if(voters > 1000) {
                voters /= 1000;
                return voters + " k voters";
            }
            return voters + " Voters";
        });

        mFillChart.setTextFormatter(progress -> {
            int cals = (int) (progress * maxCalories / 100);
            return cals + " cal";
        });

//        SpannableStringBuilder b =  new SpannableStringBuilder();
        mRingChart.setTextFormatter(progress -> {
            int days = (int) (progress * maxDays / 100);
//            b.clear();
//            String text =;
//            b.append(text);
//            int start = String.valueOf(days).length();
//            int end = b.length();
//            b.setSpan(new RelativeSizeSpan(0.4f), 0, start, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//            mText.setText(b);
            return  days + " days";
        });
    }

    private void setupColorProvider() {
        //COLOR PROVIDER
        colorProvider = new AdaptiveColorProvider() {
            @Override
            public int provideProgressColor(float progress) {
                if (progress <= 25)
                    return 0xFFF44336;
                else if (progress <= 50)
                    return 0xFFFFEA00;
                else if (progress <= 75)
                    return 0xFF03A9F4;
                else
                    return 0xFF00E676;
            }

            @Override
            public int provideBackgroundColor(float progress) {
                return ColorUtils.blendARGB(provideProgressColor(progress),
                        Color.BLACK.getValue(),
                        .8f);
            }

            @Override
            public int provideTextColor(float progress) {
                return ColorUtils.blendARGB(provideProgressColor(progress),
                        Color.WHITE.getValue(),
                        .8f);
            }

            @Override
            public int provideBackgroundBarColor(float progress) {
                return ColorUtils.blendARGB(provideProgressColor(progress),
                        Color.BLACK.getValue(),
                        .5f);
            }
        };

        mPieChart.setAdaptiveColorProvider(colorProvider);
        mRingChart.setAdaptiveColorProvider(colorProvider);
        mFillChart.setAdaptiveColorProvider(colorProvider);
    }

    //##############################################################################################   ACTIONS
    @OnClick(ResourceTable.Id_root_layout)
    public void pieChartClickAction() {
        int rand = new Random().nextInt(100);
        mPieChart.setProgress(rand, true);
        mRingChart.setProgress(rand, true);
        mFillChart.setProgress(rand, true);
    }

}
