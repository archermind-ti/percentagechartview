package com.ramijemli.sample.activity;

import butterknife.BindComponent;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.ramijemli.percentagechartview.PercentageChartView;
import com.ramijemli.percentagechartview.callback.AdaptiveColorProvider;
import com.ramijemli.sample.ResourceTable;
import com.ramijemli.sample.adapter.HomeAdapter;
import com.ramijemli.sample.util.ColorUtils;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayoutManager;
import ohos.agp.components.ListContainer;
import ohos.agp.utils.Color;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.utils.net.Uri;

import java.util.Random;


public class MainAbility extends FractionAbility {

    @BindComponent(ResourceTable.Id_list_Container)
    ListContainer mHomeRv;
    @BindComponent(ResourceTable.Id_chart)
    PercentageChartView mChart;

    private Unbinder unbinder;
    private HomeAdapter adapter;
    private EventHandler handler;
    private Runnable runnable;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_activity_home);
        unbinder = ButterKnife.bind(this,
                findComponentById(ResourceTable.Id_main_layout)
        );

        setupLayout();
        setupAnimation();
    }

    @Override
    protected void onActive() {
        super.onActive();
        animate();
    }

    @Override
    protected void onInactive() {
        handler.removeTask(runnable);
        super.onInactive();
    }

    @Override
    public void onStop() {
        handler.removeTask(runnable);
        handler = null;
        runnable = null;
        adapter.setOnHomeClickedListener(null);
        adapter = null;
        unbinder.unbind();
        unbinder = null;
        super.onStop();
    }

    private void setupLayout() {
        final DirectionalLayoutManager dlm = new DirectionalLayoutManager();
        dlm.setOrientation(Component.VERTICAL);
        mHomeRv.setLayoutManager(dlm);
        adapter = new HomeAdapter(this);
        mHomeRv.setItemProvider(adapter);

        adapter.setOnHomeClickedListener(position -> {
//            Bundle transitionbundle = ActivityOptions.makeSceneTransitionAnimation(this).toBundle();
        });

        mHomeRv.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int position, long l) {
                switch (position) {
                    default:
                    case 0:
                        startAbility(PieActivity.class);
                        break;

                    case 1:
                        startAbility(RingActivity.class);
                        break;

                    case 2:
                        startAbility(FillActivity.class);
                        break;

                    case 3:
                        startAbility(AdaptiveColorsActivity.class);
                        break;

                    case 4:
                        startAbility(GradientColorsActivity.class);
                        break;

                    case 5:
                        startAbility(TextFormatterActivity.class);
                        break;

                    case 6:
                        Intent intent2 = new Intent();
                        Operation operation = new Intent.OperationBuilder()
                                .withDeviceId("")// 跳转的目的设备，空字符串表示本机
                                .withFlags(Intent.FLAG_NOT_OHOS_COMPONENT)
                                .withUri(Uri.parse(getString(ResourceTable.String_github_url)))
                                .withAction("android.intent.action.VIEW")
                                .build();
                        intent2.setOperation(operation);
                        getContext().startAbility(intent2, 0);
                        break;
                }
            }
        });

        adapter.notifyDataChanged();

    }

    private void startAbility(Class abilityClazz) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(getContext().getBundleName())
                .withAbilityName(abilityClazz.getName())
                .build();
        intent.setOperation(operation);
        startAbility(intent);
    }

    private void setupAnimation() {
//        mChart.setAnimationInterpolator(new AnticipateOvershootInterpolator());
        mChart.setAnimationDuration(600);
        mChart.setAdaptiveColorProvider(new AdaptiveColorProvider() {
            @Override
            public int provideProgressColor(float progress) {
                String color;

                if (progress <= 25)
                    color = "#F44336";
                else if (progress <= 50)
                    color = "#9C27B0";
                else if (progress <= 75)
                    color = "#03A9F4";
                else color = "#FFC107";

                return Color.getIntColor(color);
            }

            @Override
            public int provideTextColor(float progress) {
                return ColorUtils.blendARGB(provideProgressColor(progress), Color.WHITE.getValue(), .6f);
            }
        });

        handler = new EventHandler(EventRunner.getMainEventRunner());
        runnable = () -> {
            mChart.setProgress(new Random().nextInt(100), true);
            animate();
        };
        animate();
    }

    private void animate() {
        handler.postTask(runnable, 1500);
    }

}
