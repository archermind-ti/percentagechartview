package com.ramijemli.sample.activity;

import butterknife.BindComponent;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ramijemli.percentagechartview.PercentageChartView;
import com.ramijemli.sample.ResourceTable;
import com.ramijemli.sample.adapter.ProjectPageSliderProvider;
import com.ramijemli.sample.fragment.*;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.agp.components.PageSlider;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.ramijemli.sample.fragment.BackgroundSubFragment.OFFSET_STATE_ARG;
import static com.ramijemli.sample.fragment.BehaviorSubFragment.ORIENTATION_STATE_ARG;
import static com.ramijemli.sample.fragment.ProgressSubFragment.BAR_STATE_ARG;


public class RingActivity extends FractionAbility implements BehaviorSubFragment.OnBehaviorChangedListener,
        ProgressSubFragment.OnProgressChangedListener,
        BackgroundSubFragment.OnBackgroundChangedListener,
        BackgroundBarSubFragment.OnBackgroundBarChangedListener,
        TextSubFragment.OnTextChangedListener {

    @BindComponent(ResourceTable.Id_chart)
    PercentageChartView mChart;
    @BindComponent(ResourceTable.Id_page_slider)
    PageSlider pageSlider;
    @BindComponent(ResourceTable.Id_tabs)
    SmartTabLayout mTAbs;

    private Unbinder unbinder;
    private int shadowColor;
    private float blur, distX, distY;
    private boolean draw;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_activity_ring);
        unbinder = ButterKnife.bind(this,
                findComponentById(ResourceTable.Id_main_layout)
        );

        mTAbs.setCustomTabView(ResourceTable.Layout_custom_tab, ResourceTable.Id_tab_text);

        setupLayout();
    }

    @Override
    public void onStop() {
        unbinder.unbind();
        unbinder = null;
        super.onStop();
    }

    private void setupLayout() {

        List<ProjectPageSliderProvider.FractionWrapper> list = new ArrayList<>();
        list.add(new ProjectPageSliderProvider.FractionWrapper("behavior",
                BehaviorSubFragment.class,
                new Intent().setParam(ORIENTATION_STATE_ARG, true))
        );

        list.add(new ProjectPageSliderProvider.FractionWrapper("progress bar",
                ProgressSubFragment.class,
                new Intent().setParam(BAR_STATE_ARG, true))
        );

        list.add(new ProjectPageSliderProvider.FractionWrapper("background bar",
                BackgroundBarSubFragment.class)
        );

        list.add(new ProjectPageSliderProvider.FractionWrapper("background",
                BackgroundSubFragment.class,
                new Intent().setParam(OFFSET_STATE_ARG, false))
        );

        list.add(new ProjectPageSliderProvider.FractionWrapper("text",
                TextSubFragment.class)
        );

        pageSlider.setProvider(new ProjectPageSliderProvider(list));
//        mViewPager.setOffscreenPageLimit(4);
        mTAbs.setViewPager(pageSlider);

        shadowColor = Color.WHITE.getValue();
        blur = distX = distY = 2f;
    }

    //##############################################################################################   CALLBACKS
    @Override
    public void onOrientationChanged(int orientation) {
        mChart.setOrientation(orientation);
    }

    @Override
    public void onStartAngleChanged(int angle) {
        mChart.setStartAngle(angle);
    }

    @Override
    public void onAnimDurationChanged(int duration) {
        mChart.setAnimationDuration(duration);
    }

    @Override
    public void onInterpolatorChanged(int curveType) {
        mChart.setAnimationInterpolator(curveType);
    }

    @Override
    public void onProgressChanged(float progress, boolean animate) {
        mChart.setProgress(progress, animate);
    }

    @Override
    public void onProgressColorChanged(int color) {
        mChart.setProgressColor(color);
    }

    @Override
    public void onDrawBackgroundChanged(boolean draw) {
        mChart.setDrawBackgroundEnabled(draw);
    }

    @Override
    public void onBackgroundColorChanged(int color) {
        mChart.setBackgroundColor(color);
    }

    @Override
    public void onBackgroundOffsetChanged(int offset) {
        mChart.setBackgroundOffset(offset);
    }

    @Override
    public void onTextColorChanged(int color) {
        mChart.setTextColor(color);
    }

    @Override
    public void onTextSizeChanged(int textSize) {
        mChart.setTextSize(textSize);
    }

    @Override
    public void onTextFontChanged(Font.Builder typeface) {
        mChart.setTypeface(typeface);
    }

    @Override
    public void onTextStyleChanged(int textStyle) {
        mChart.setTextStyle(textStyle);
    }

    @Override
    public void onDrawShadowChanged(boolean draw) {
        this.draw = draw;
        if (draw && shadowColor != 0) {
            mChart.setTextShadow(shadowColor, blur, distX, distY);
        } else {
            mChart.setTextShadow(0, 0, 0, 0);
        }
    }

    @Override
    public void onShadowChanged(int color, float blur, float distX, float distY) {
        this.shadowColor = color;
        this.blur = blur;
        this.distX = distX;
        this.distY = distY;
        if (draw && shadowColor != 0) {
            mChart.setTextShadow(color, blur, distX, distY);
        }else{
            mChart.setTextShadow(0, 0, 0, 0);
        }
    }

    @Override
    public void onProgBarThicknessChanged(int thickness) {
        mChart.setProgressBarThickness(thickness);
    }

    @Override
    public void onProgBarStyleChanged(int progressStyle) {
        mChart.setProgressBarStyle(progressStyle);
    }


    @Override
    public void onDrawBgBarChanged(boolean draw) {
        mChart.setDrawBackgroundBarEnabled(draw);
    }

    @Override
    public void onBgBarColorChanged(int color) {
        mChart.setBackgroundBarColor(color);
    }

    @Override
    public void onBgBarThicknessChanged(int thickness) {
        mChart.setBackgroundBarThickness(thickness);
    }

    //##############################################################################################   ACTIONS
    @OnClick(ResourceTable.Id_chart)
    public void chartClickAction() {
        mChart.setProgress(new Random().nextInt(100), true);
    }

}
