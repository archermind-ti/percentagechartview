package com.ramijemli.sample.fragment;

import butterknife.BindComponent;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.ramijemli.percentagechartview.annotation.ProgressOrientation;
import com.ramijemli.sample.ResourceTable;
import com.ramijemli.sample.util.FractionInterface;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.Animator;
import ohos.agp.components.*;
import ohos.agp.window.dialog.ToastDialog;

import java.util.ArrayList;
import java.util.List;

import static com.ramijemli.percentagechartview.utils.MyAttrsUtils.*;

public class BehaviorSubFragment extends Fraction implements FractionInterface {

    public static final String ORIENTATION_STATE_ARG = "BehaviorSubFragment.ORIENTATION_STATE_ARG";

    //ORIENTATION
    @BindComponent(ResourceTable.Id_orientation_value)
    RadioContainer mOrientationValue;
    @BindComponent(ResourceTable.Id_orientation_visibility)
    ComponentContainer mOrientationGroup;

    //START ANGLE
    @BindComponent(ResourceTable.Id_start_angle_value)
    Slider mStartAngleValue;
    @BindComponent(ResourceTable.Id_start_angle_value_label)
    Text mStartAngleLabel;

    //DURATION
    @BindComponent(ResourceTable.Id_duration_value)
    Text mDurationValue;

    //INTERPOLATOR
    @BindComponent(ResourceTable.Id_interpolator_value)
    Picker mInterpolatorValue;

    private OnBehaviorChangedListener mListener;
    private Unbinder unbinder;
    private boolean enableOrientation;
    private Component component;

    public BehaviorSubFragment() {
    }

    public static BehaviorSubFragment newInstance() {
        return new BehaviorSubFragment();
    }

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        if (intent != null) {
            enableOrientation = intent.getBooleanParam(ORIENTATION_STATE_ARG, false);
        }
    }

    @Override
    public Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        if (container.getContext() instanceof OnBehaviorChangedListener) {
            mListener = (OnBehaviorChangedListener) container.getContext();
        } else {
            throw new RuntimeException(container.getContext().toString()
                    + " must implement OnBackgroundBarChangedListener");
        }

        component = scatter.parse(ResourceTable.Layout_sub_fragment_behavior, container, false);
        unbinder = ButterKnife.bind(this, component);
        if (enableOrientation) {
            mOrientationGroup.setVisibility(Component.VISIBLE);
            setupOrientation();
        } else {
            mOrientationGroup.setVisibility(Component.HIDE);
        }
        setupStartAngle();
        setupInterpolator();
        return component;
    }

    @Override
    public void onComponentDetach() {
        super.onComponentDetach();
        mListener = null;
    }

    @Override
    public void onStop() {
        mOrientationValue.setMarkChangedListener(null);
        mInterpolatorValue.setValueChangedListener(null);
        mStartAngleValue.setValueChangedListener(null);
        unbinder.unbind();
        unbinder = null;
        super.onStop();
    }


    //##############################################################################################   BEHAVIOR
    private void setupOrientation() {
        mOrientationValue.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(RadioContainer radioContainer, int i) {

            }
        });
        mOrientationValue.setMarkChangedListener((radioContainer, i) -> {
            if (mListener != null) {
                mListener.onOrientationChanged(radioContainer.getComponentAt(i).getId() == ResourceTable.Id_clockwise ? ORIENTATION_CLOCKWISE : ORIENTATION_COUNTERCLOCKWISE);
            }
        });
    }

    private void setupStartAngle() {
        mStartAngleValue.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                if (mListener != null) {
                    mListener.onStartAngleChanged(i);
                }
                mStartAngleLabel.setText(String.valueOf(i));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });
    }

    private void setupInterpolator() {

        List<String> data = new ArrayList<>();
        data.add("LINEAR");//LINEAR
        data.add("ACCELERATE");//ACCELERATE
        data.add("DECELERATE");//DECELERATE
        data.add("ACCELERATE_DECELERATE");//ACCELERATE_DECELERATE
        data.add("ANTICIPATE");//ANTICIPATE
        data.add("OVERSHOOT");//OVERSHOOT
        data.add("ANTICIPATE_OVERSHOOT");//ANTICIPATE_OVERSHOOT
        data.add("BOUNCE");//BOUNCE
//        data.add("FAST_OUT_LINEAR_IN");
//        data.add("FAST_OUT_SLOW_IN");
//        data.add("LINEAR_OUT_SLOW_IN");

        mInterpolatorValue.setDisplayedData(data.toArray(new String[0]));
        mInterpolatorValue.setValue(0);
        mInterpolatorValue.setMaxValue(data.size() - 1);
        mInterpolatorValue.setValueChangedListener(new Picker.ValueChangedListener() {
            @Override
            public void onValueChanged(Picker picker1, int i, int i1) {
                if (i == i1) {
                    return;
                }
                int interpolator = -1;
                if (mListener != null) {
                    switch (i1) {
                        case LINEAR:
                            interpolator = Animator.CurveType.LINEAR;
                            break;
                        case ACCELERATE:
                            interpolator = Animator.CurveType.ACCELERATE;
                            break;
                        case DECELERATE:
                            interpolator = Animator.CurveType.DECELERATE;
                            break;
                        case ACCELERATE_DECELERATE:
                            interpolator = Animator.CurveType.ACCELERATE_DECELERATE;
                            break;
                        case ANTICIPATE:
                            interpolator = Animator.CurveType.ANTICIPATE;
                            break;
                        case OVERSHOOT:
                            interpolator = Animator.CurveType.OVERSHOOT;
                            break;
                        case ANTICIPATE_OVERSHOOT:
                            interpolator = Animator.CurveType.ANTICIPATE_OVERSHOOT;
                            break;
                        case BOUNCE:
                            interpolator = Animator.CurveType.BOUNCE;
                            break;
                    }
                    if (mListener != null) {
                        mListener.onInterpolatorChanged(interpolator);
                    }
                }
            }
        });
    }

    private void tweakAnimDuration(int amount) {
        int value = getTextViewValue(mDurationValue);

        if (value + amount > 4000) {
            value += amount;
            new ToastDialog(this).setText("Welcome to the boring animation hell!").show();
        } else if (value + amount <= 0) {
            value = 100;
        } else {
            value += amount;
        }

        mDurationValue.setText(value + " ms");
        if (mListener != null) {
            mListener.onAnimDurationChanged(value);
        }
    }

    private int getTextViewValue(Text view) {
        String value = view.getText().toString();
        return Integer.parseInt(value.substring(0, value.length() - 3));
    }

    //##############################################################################################   ACTIONS
    @OnClick(ResourceTable.Id_increment_duration)
    void incrementAction() {
        tweakAnimDuration(100);
    }

    @OnClick(ResourceTable.Id_decrement_duration)
    void decrementAction() {
        tweakAnimDuration(-100);
    }

    //##############################################################################################   LISTENER
    public interface OnBehaviorChangedListener {
        void onOrientationChanged(@ProgressOrientation int orientation);

        void onStartAngleChanged(int angle);

        void onAnimDurationChanged(int duration);

        void onInterpolatorChanged(int curveType);
    }
}
