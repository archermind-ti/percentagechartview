package com.ramijemli.sample.fragment;

import butterknife.BindComponent;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.ramijemli.percentagechartview.renderer.RingModeRenderer;
import com.ramijemli.sample.ResourceTable;
import com.ramijemli.sample.util.FractionInterface;
import com.ramijemli.sample.util.ScreenUtil;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;


public class ProgressSubFragment extends Fraction implements FractionInterface {

    public static final String BAR_STATE_ARG = "ProgressSubFragment.BAR_STATE_ARG";

    //PROGRESS
    @BindComponent(ResourceTable.Id_progress_value)
    Slider mProgressValue;
    @BindComponent(ResourceTable.Id_progress_value_label)
    Text mProgressLabel;
    @BindComponent(ResourceTable.Id_animate)
    Checkbox mAnimateProgress;

    //PROGRESS COLOR
    @BindComponent(ResourceTable.Id_progress_color)
    Button mProgressColor;

    //PROGRESS BAR THICKNESS
    @BindComponent(ResourceTable.Id_prog_thickness_value)
    Text mPgBarThicknessValue;

    //PROGRESS BAR STYLE
    @BindComponent(ResourceTable.Id_prog_bar_style_value)
    RadioContainer mPgBarStyle;

    @BindComponent(ResourceTable.Id_bar_visibility)
    ComponentContainer mBarGroup;

    private OnProgressChangedListener mListener;
    private Unbinder unbinder;
    private boolean enableBar;
    private Context mContext;

    public ProgressSubFragment() {
    }

    public static ProgressSubFragment newInstance() {
        return new ProgressSubFragment();
    }

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        if (intent != null) {
            enableBar = intent.getBooleanParam(BAR_STATE_ARG, false);
        }
    }


    @Override
    public Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        mContext = container.getContext();
        if (container.getContext() instanceof OnProgressChangedListener) {
            mListener = (OnProgressChangedListener) container.getContext();
        } else {
            throw new RuntimeException(container.getContext().toString()
                    + " must implement OnBackgroundBarChangedListener");
        }

        Component component = scatter.parse(ResourceTable.Layout_sub_fragment_progress, container, false);
        unbinder = ButterKnife.bind(this, component);

        setupProgress();
        if (enableBar) {
            mBarGroup.setVisibility(Component.VISIBLE);
            setupPgBarStyle();
        } else {
            mBarGroup.setVisibility(Component.HIDE);
        }
        return component;
    }

    @Override
    public void onComponentDetach() {
        super.onComponentDetach();
        mListener = null;
    }

    @Override
    public void onStop() {
        mProgressValue.setValueChangedListener(null);
        mAnimateProgress.setCheckedStateChangedListener(null);
        mPgBarStyle.setMarkChangedListener(null);
        unbinder.unbind();
        unbinder = null;
        super.onStop();
    }


    //##############################################################################################   BEHAVIOR
    private void setupProgress() {
        mProgressValue.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                mProgressLabel.setText(String.valueOf(i));

                if (mAnimateProgress.isChecked()) return;

                if (mListener != null) {
                    mListener.onProgressChanged(i, false);
                }
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {
                if (mAnimateProgress.isChecked()) {
                    if (mListener != null) {
                        mListener.onProgressChanged(slider.getProgress(), true);
                    }
                }
                mProgressLabel.setText(String.valueOf(slider.getProgress()));
            }
        });
    }

    private void setupPgBarStyle() {
        mPgBarStyle.setMarkChangedListener((radioContainer, i) -> {
            if (mListener == null) return;
            switch (radioContainer.getComponentAt(i).getId()) {
                case ResourceTable.Id_round:
                    mListener.onProgBarStyleChanged(RingModeRenderer.CAP_ROUND);
                    break;
                case ResourceTable.Id_square:
                    mListener.onProgBarStyleChanged(RingModeRenderer.CAP_SQUARE);
                    break;
            }
        });
    }

    private void tweakPgThickness(int amount) {
        int value = getTextViewValue(mPgBarThicknessValue);
        int maxValue = (int) ScreenUtil.convertPixelsToDIP(this, getComponent().getEstimatedWidth());
        if (value + amount > maxValue) {
            value = maxValue;
        } else if (value + amount <= 0) {
            value = 0;
        } else {
            value += amount;
        }

        mPgBarThicknessValue.setText(value + " vp");
        if (mListener != null) {
            mListener.onProgBarThicknessChanged(ScreenUtil.convertDIPToPixels(this, value));
        }
    }

    private int getTextViewValue(Text view) {
        String value = view.getText();
        return Integer.parseInt(value.substring(0, value.length() - 3));
    }

    //##############################################################################################   ACTIONS
    @OnClick(ResourceTable.Id_progress_color)
    void progressAction() {
        ColorPickerDialogBuilder
                .with(mContext, 2)
                .setTitle("Choose progress color")
                .setStats(1)
                .setSelectColorInterface(new OnColorSelectedListener() {

                    @Override
                    public void onColorSelected(int i) {

                    }

                    @Override
                    public void onColorMakesure(int i) {
                        ShapeElement shapeElement = new ShapeElement();
                        shapeElement.setRgbColor(RgbColor.fromArgbInt(i));
                        mProgressColor.setBackground(shapeElement);
                        if (mListener != null) {
                            mListener.onProgressColorChanged(i);
                        }
                    }
                });
    }

    @OnClick(ResourceTable.Id_increment_prog_thickness)
    void incrementPgThicknessAction() {
        tweakPgThickness(1);
    }

    @OnClick(ResourceTable.Id_decrement_prog_thickness)
    void decrementPgThicknessAction() {
        tweakPgThickness(-1);
    }

    //##############################################################################################   LISTENER
    public interface OnProgressChangedListener {
        void onProgressChanged(float progress, boolean animate);

        void onProgressColorChanged(int color);

        default void onProgBarThicknessChanged(int thickness) {
        }

        default void onProgBarStyleChanged(int progressStyle) {
        }
    }
}
