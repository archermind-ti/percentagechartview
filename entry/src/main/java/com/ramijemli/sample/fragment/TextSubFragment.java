package com.ramijemli.sample.fragment;

import butterknife.BindComponent;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.ramijemli.percentagechartview.utils.FontUtils;
import com.ramijemli.sample.ResourceTable;
import com.ramijemli.sample.util.FractionInterface;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

import static com.ramijemli.percentagechartview.utils.MyAttrsUtils.*;

public class TextSubFragment extends Fraction implements FractionInterface {

    //TEXT COLOR
    @BindComponent(ResourceTable.Id_text_color)
    Button mTextColor;

    //TEXT SIZE
    @BindComponent(ResourceTable.Id_text_size_value)
    Text mTextSizeValue;

    //TEXT FONT
    @BindComponent(ResourceTable.Id_font_value)
    Picker mFontValue;

    //TEXT STYLE
    @BindComponent(ResourceTable.Id_text_style_value)
    Picker mTextStyleValue;

    //USE SHADOW STATE
    @BindComponent(ResourceTable.Id_use_shadow)
    Checkbox mUseShadow;

    //SHADOW COLOR
    @BindComponent(ResourceTable.Id_shadow_color)
    Button mShadowColor;

    //SHADOW RADIUS
    @BindComponent(ResourceTable.Id_radius_value)
    Text mRadiusValue;

    //SHADOW DIST X
    @BindComponent(ResourceTable.Id_distx_value)
    Text mDistXValue;

    //SHADOW DIST Y
    @BindComponent(ResourceTable.Id_disty_value)
    Text mDistYValue;

    private OnTextChangedListener mListener;
    private Unbinder unbinder;
    private int shadowColor;
    private Context mContext;

    public TextSubFragment() {
    }

    public static TextSubFragment newInstance() {
        return new TextSubFragment();
    }

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
    }

    @Override
    public Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        mContext = container.getContext();
        if (container.getContext() instanceof OnTextChangedListener) {
            mListener = (OnTextChangedListener) container.getContext();
        } else {
            throw new RuntimeException(container.getContext().toString()
                    + " must implement OnBackgroundBarChangedListener");
        }

        Component component = scatter.parse(ResourceTable.Layout_sub_fragment_text, container, false);
        unbinder = ButterKnife.bind(this, component);

        setupTextFont();
        setupTextStyle();
        setupTextShadow();
        return component;
    }

    @Override
    public void onComponentDetach() {
        super.onComponentDetach();
        mListener = null;
    }

    @Override
    public void onStop() {
        mFontValue.setValueChangedListener(null);
        mTextStyleValue.setValueChangedListener(null);
        mUseShadow.setCheckedStateChangedListener(null);
        unbinder.unbind();
        unbinder = null;
        super.onStop();
    }

    //##############################################################################################   BEHAVIOR
    private void setupTextFont() {
        List<String> data = new ArrayList<>();
        data.add("SYSTEM FONT (DEFAULT)");
        data.add("INTERSTELLAR");
        data.add("MARSHMELLOWS");
        data.add("NUAZ");
        data.add("WHALE I TRIED");

        mFontValue.setDisplayedData(data.toArray(new String[0]));
        mFontValue.setValue(0);
        mFontValue.setMaxValue(4);
        mFontValue.setValueChangedListener(new Picker.ValueChangedListener() {
            @Override
            public void onValueChanged(Picker picker, int i, int i1) {
                if (i == i1) {
                    return;
                }
                String font = null;
                switch (i1) {
                    case 0:
                        font = "HwChinese-medium";
                        break;
                    case 1:
                        font = "interstellar.ttf";
                        break;
                    case 2:
                        font = "marshmallows.ttf";
                        break;
                    case 3:
                        font = "nuaz.otf";
                        break;
                    case 4:
                        font = "whaleitried.ttf";
                        break;
                }

                Font.Builder typeface = FontUtils.getFontBuilder(mContext.getApplicationContext(), font);
                if (mListener != null) {
                    mListener.onTextFontChanged(typeface);
                }
            }
        });
    }

    private void setupTextStyle() {
        List<String> data = new ArrayList<>();
        data.add("REGULAR");
        data.add("BOLD");
        data.add("ITALIC");
        data.add("BOLD_ITALIC");
        mTextStyleValue.setDisplayedData(data.toArray(new String[0]));
        mTextStyleValue.setValue(0);
        mTextStyleValue.setMaxValue(3);
        mTextStyleValue.setValueChangedListener((Picker picker, int i, int i1) -> {
            if (i == i1) {
                return;
            }
            int textStyle = -1;
            switch (i1) {
                case 0:
                    textStyle = NORMAL;
                    break;
                case 1:
                    textStyle = BOLD;
                    break;
                case 2:
                    textStyle = ITALIC;
                    break;
                case 3:
                    textStyle = BOLD_ITALIC;
                    break;
            }

            if (mListener != null) {
                mListener.onTextStyleChanged(textStyle);
            }

        });
    }

    private void setupTextShadow() {
        mUseShadow.setCheckedStateChangedListener((buttonView, isChecked) -> {
            if (mListener != null) {
                mListener.onDrawShadowChanged(isChecked);
            }
        });

        shadowColor = Color.WHITE.getValue();
    }

    private void tweakTextSize(int size) {
        int value = getTextViewValue(mTextSizeValue);

        if (value + size > 100) {
            value = 100;
        } else if (value + size <= 10) {
            value = 10;
        } else {
            value += size;
        }

        mTextSizeValue.setText(value + " fp");

        if (mListener != null) {
            mListener.onTextSizeChanged(AttrHelper.fp2px(value, getContext()));
        }
    }

    private void tweakShadowRadius(int amount) {
        float value = Float.parseFloat(mRadiusValue.getText());

        if (value + amount > 100) {
            value = 100;
        } else if (value + amount < 1) {
            value = 1;
        } else {
            value += amount;
        }

        mRadiusValue.setText(String.valueOf(value));
        if (mListener != null) {
            mListener.onShadowChanged(shadowColor,
                    value,
                    Float.parseFloat(mDistXValue.getText()),
                    Float.parseFloat(mDistYValue.getText())
            );
        }
    }

    private void tweakDistX(int amount) {
        float value = Float.parseFloat(mDistXValue.getText());

        if (value + amount > 36) {
            value = 36;
        } else if (value + amount < 0) {
            value = 0;
        } else {
            value += amount;
        }

        mDistXValue.setText(String.valueOf(value));
        if (mListener != null) {
            mListener.onShadowChanged(shadowColor,
                    Float.parseFloat(mRadiusValue.getText()),
                    value,
                    Float.parseFloat(mDistYValue.getText())
            );
        }
    }

    private void tweakDistY(int amount) {
        float value = Float.parseFloat(mDistYValue.getText());

        if (value + amount > 36) {
            value = 36;
        } else if (value + amount < 0) {
            value = 0;
        } else {
            value += amount;
        }

        mDistYValue.setText(String.valueOf(value));
        if (mListener != null) {
            mListener.onShadowChanged(shadowColor,
                    Float.parseFloat(mRadiusValue.getText()),
                    Float.parseFloat(mDistXValue.getText()),
                    value);
        }
    }

    private int getTextViewValue(Text view) {
        String value = view.getText();
        return Integer.parseInt(value.substring(0, value.length() - 3));
    }

    //##############################################################################################   ACTIONS
    @OnClick(ResourceTable.Id_text_color)
    void textColorAction() {
        ColorPickerDialogBuilder
                .with(mContext, 2)
                .setTitle("Choose text color")
                .setStats(1)
                .setSelectColorInterface(new OnColorSelectedListener() {

                    @Override
                    public void onColorSelected(int i) {

                    }

                    @Override
                    public void onColorMakesure(int i) {
                        ShapeElement shapeElement = new ShapeElement();
                        shapeElement.setRgbColor(RgbColor.fromArgbInt(i));
                        mTextColor.setBackground(shapeElement);
                        if (mListener != null) {
                            mListener.onTextColorChanged(i);
                        }
                    }
                });
    }

    @OnClick(ResourceTable.Id_increment_text_size)
    void incrementTextSizeAction() {
        tweakTextSize(2);
    }

    @OnClick(ResourceTable.Id_decrement_text_size)
    void decrementTextSizeAction() {
        tweakTextSize(-2);
    }

    @OnClick(ResourceTable.Id_shadow_color)
    void shadowColorAction() {
        ColorPickerDialogBuilder
                .with(mContext, 2)
                .setTitle("Choose shadow color")
                .setStats(1)
                .setSelectColorInterface(new OnColorSelectedListener() {

                    @Override
                    public void onColorSelected(int i) {

                    }

                    @Override
                    public void onColorMakesure(int i) {
                        shadowColor = i;
                        ShapeElement shapeElement = new ShapeElement();
                        shapeElement.setRgbColor(RgbColor.fromArgbInt(shadowColor));
                        mShadowColor.setBackground(shapeElement);
                        if (mListener != null) {
                            mListener.onShadowChanged(shadowColor,
                                    Float.parseFloat(mRadiusValue.getText().toString()),
                                    Float.parseFloat(mDistXValue.getText().toString()),
                                    Float.parseFloat(mDistYValue.getText().toString()));
                        }
                    }
                });
    }

    @OnClick(ResourceTable.Id_increment_shadow)
    void incrementShadowAction() {
        tweakShadowRadius(1);
    }

    @OnClick(ResourceTable.Id_decrement_shadow)
    void decrementShadowAction() {
        tweakShadowRadius(-1);
    }

    @OnClick(ResourceTable.Id_increment_distx)
    void incrementDistXAction() {
        tweakDistX(1);
    }

    @OnClick(ResourceTable.Id_decrement_distx)
    void decrementDistXAction() {
        tweakDistX(-1);
    }

    @OnClick(ResourceTable.Id_increment_disty)
    void incrementDistYAction() {
        tweakDistY(1);
    }

    @OnClick(ResourceTable.Id_decrement_disty)
    void decrementDistYAction() {
        tweakDistY(-1);
    }

    //##############################################################################################   LISTENER
    public interface OnTextChangedListener {
        void onTextColorChanged(int color);

        void onTextSizeChanged(int textSize);

        void onTextFontChanged(Font.Builder typeface);

        void onTextStyleChanged(int textStyle);

        void onDrawShadowChanged(boolean draw);

        void onShadowChanged(int color, float blur, float distX, float distY);
    }
}
