package com.ramijemli.sample.fragment;

import butterknife.BindComponent;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.ramijemli.sample.ResourceTable;
import com.ramijemli.sample.util.FractionInterface;
import com.ramijemli.sample.util.ScreenUtil;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;


public class BackgroundSubFragment extends Fraction implements FractionInterface {

    public static final String OFFSET_STATE_ARG = "BackgroundSubFragment.OFFSET_STATE_ARG";

    //DRAW BACKGROUND STATE
    @BindComponent(ResourceTable.Id_draw_background)
    Checkbox mDrawBackground;

    //BACKGROUND COLOR
    @BindComponent(ResourceTable.Id_background_color)
    Button mBackgroundColor;

    //BACKGROUND OFFSET
    @BindComponent(ResourceTable.Id_offset_value)
    Text mOffsetValue;
    @BindComponent(ResourceTable.Id_offset_visibility)
    ComponentContainer mOffsetGroup;

    private OnBackgroundChangedListener mListener;
    private Unbinder unbinder;
    private boolean enableOffset;
    private Context mContext;

    public BackgroundSubFragment() {
    }

    public static BackgroundSubFragment newInstance() {
        return new BackgroundSubFragment();
    }

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        if (intent != null) {
            enableOffset = intent.getBooleanParam(OFFSET_STATE_ARG, false);
        }
    }

    @Override
    public Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        mContext = container.getContext();
        if (container.getContext() instanceof OnBackgroundChangedListener) {
            mListener = (OnBackgroundChangedListener) container.getContext();
        } else {
            throw new RuntimeException(container.getContext().toString()
                    + " must implement OnBackgroundBarChangedListener");
        }

        Component component = scatter.parse(ResourceTable.Layout_sub_fragment_background, container, false);
        unbinder = ButterKnife.bind(this, component);

        setupDrawBackgroundState();
        if (enableOffset) {
            mOffsetGroup.setVisibility(Component.VISIBLE);
        } else {
            mOffsetGroup.setVisibility(Component.HIDE);
        }
        return component;
    }

    @Override
    public void onComponentDetach() {
        super.onComponentDetach();
        mListener = null;
    }

    @Override
    public void onStop() {
        mDrawBackground.setCheckedStateChangedListener(null);
        unbinder.unbind();
        unbinder = null;
        super.onStop();
    }

    //##############################################################################################   BEHAVIOR
    private void setupDrawBackgroundState() {
        mDrawBackground.setCheckedStateChangedListener((buttonView, isChecked) -> {
            String text = String.valueOf(isChecked);
            mDrawBackground.setText(Character.toUpperCase(text.charAt(0)) + text.substring(1).toLowerCase());
            if (mListener != null) {
                mListener.onDrawBackgroundChanged(isChecked);
            }
        });
    }

    private void tweakOffset(int amount) {
        int value = getTextViewValue(mOffsetValue);

        int maxValue = (int) ScreenUtil.convertPixelsToDIP(this, 400);
        if (value + amount > maxValue) {
            value = maxValue;
        } else if (value + amount <= 0) {
            value = 0;
        } else {
            value += amount;
        }

        mOffsetValue.setText(value + " vp");

        if (mListener != null) {
            mListener.onBackgroundOffsetChanged(ScreenUtil.convertDIPToPixels(this, value));
        }
    }

    private int getTextViewValue(Text view) {
        String value = view.getText();
        return Integer.parseInt(value.substring(0, value.length() - 3));
    }

    //##############################################################################################   ACTIONS
    @OnClick(ResourceTable.Id_background_color)
    void backgroundAction() {
        ColorPickerDialogBuilder
                .with(mContext, 2)
                .setTitle("Choose background color")
                .setStats(1)
                .setSelectColorInterface(new OnColorSelectedListener() {

                    @Override
                    public void onColorSelected(int i) {

                    }

                    @Override
                    public void onColorMakesure(int i) {
                        ShapeElement shapeElement = new ShapeElement();
                        shapeElement.setRgbColor(RgbColor.fromArgbInt(i));
                        mBackgroundColor.setBackground(shapeElement);
                        if (mListener != null) {
                            mListener.onBackgroundColorChanged(i);
                        }
                    }
                });
    }

    @OnClick(ResourceTable.Id_increment_offset)
    void incrementOffsetAction() {
        tweakOffset(1);
    }

    @OnClick(ResourceTable.Id_decrement_offset)
    void decrementOffsetAction() {
        tweakOffset(-1);
    }

    //##############################################################################################   LISTENER
    public interface OnBackgroundChangedListener {
        void onDrawBackgroundChanged(boolean draw);

        void onBackgroundColorChanged(int color);

        void onBackgroundOffsetChanged(int offset);
    }
}
