package com.ramijemli.sample.fragment;

import butterknife.BindComponent;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.ramijemli.sample.ResourceTable;
import com.ramijemli.sample.util.FractionInterface;
import com.ramijemli.sample.util.ScreenUtil;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;


public class BackgroundBarSubFragment extends Fraction implements FractionInterface {


    //DRAW BACKGROUND BAR STATE
    @BindComponent(ResourceTable.Id_draw_bg_bar)
    Checkbox mDrawBgBar;

    //BACKGROUND BAR COLOR
    @BindComponent(ResourceTable.Id_bg_bar_color)
    Button mBgBarColor;

    //BACKGROUND BAR THICKNESS
    @BindComponent(ResourceTable.Id_bg_bar_thickness_value)
    Text mBgBarThicknessValue;


    private OnBackgroundBarChangedListener mListener;
    private Unbinder unbinder;
    private Context mContext;

    public BackgroundBarSubFragment() {
    }

    public static BackgroundBarSubFragment newInstance() {
        return new BackgroundBarSubFragment();
    }

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
    }

    @Override
    public Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        mContext = container.getContext();
        if (container.getContext() instanceof OnBackgroundBarChangedListener) {
            mListener = (OnBackgroundBarChangedListener) container.getContext();
        } else {
            throw new RuntimeException(container.getContext().toString()
                    + " must implement OnBackgroundBarChangedListener");
        }

        Component component = scatter.parse(ResourceTable.Layout_sub_fragment_background_bar, container, false);
        unbinder = ButterKnife.bind(this, component);
        setupDrawBgBarState();
        return component;
    }

    @Override
    public void onComponentDetach() {
        super.onComponentDetach();
        mListener = null;
    }

    @Override
    public void onStop() {
        mDrawBgBar.setCheckedStateChangedListener(null);
        unbinder.unbind();
        unbinder = null;
        super.onStop();
    }

    //##############################################################################################   BEHAVIOR
    private void setupDrawBgBarState() {
        mDrawBgBar.setCheckedStateChangedListener((buttonView, isChecked) -> {
            String text = String.valueOf(isChecked);
            mDrawBgBar.setText(Character.toUpperCase(text.charAt(0)) + text.substring(1).toLowerCase());
            if (mListener != null) {
                mListener.onDrawBgBarChanged(isChecked);
            }
        });
    }

    private void tweakBgThickness(int amount) {
        int value = getTextViewValue(mBgBarThicknessValue);

        int maxValue = (int) ScreenUtil.convertPixelsToDIP(this, 400);
        if (value + amount > maxValue) {
            value = maxValue;
        } else if (value + amount <= 0) {
            value = 0;
        } else {
            value += amount;
        }

        mBgBarThicknessValue.setText(value + " vp");
        if (mListener != null) {
            mListener.onBgBarThicknessChanged(AttrHelper.vp2px(value, mContext));
        }
    }

    private int getTextViewValue(Text view) {
        String value = view.getText();
        return Integer.parseInt(value.substring(0, value.length() - 3));
    }

    //##############################################################################################   ACTIONS
    @OnClick(ResourceTable.Id_bg_bar_color)
    void bgBarColorAction() {
        ColorPickerDialogBuilder
                .with(mContext, 2)
                .setTitle("Choose Background bar color")
                .setStats(1)
                .setSelectColorInterface(new OnColorSelectedListener() {
                    @Override
                    public void onColorSelected(int i) {

                    }

                    @Override
                    public void onColorMakesure(int i) {
                        ShapeElement shapeElement = new ShapeElement();
                        shapeElement.setRgbColor(RgbColor.fromArgbInt(i));
                        mBgBarColor.setBackground(shapeElement);
                        if (mListener != null) {
                            mListener.onBgBarColorChanged(i);
                        }
                    }
                });
    }

    @OnClick(ResourceTable.Id_increment_bg_bar_thickness)
    void incrementBgThicknessAction() {
        tweakBgThickness(1);
    }

    @OnClick(ResourceTable.Id_decrement_bg_bar_thickness)
    void decrementBgThicknessAction() {
        tweakBgThickness(-1);
    }

    //##############################################################################################   LISTENER
    public interface OnBackgroundBarChangedListener {
        void onDrawBgBarChanged(boolean draw);

        void onBgBarColorChanged(int color);

        void onBgBarThicknessChanged(int thickness);
    }
}
